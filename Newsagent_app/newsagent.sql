
-- create database
DROP DATABASE IF EXISTS newsagent;
CREATE DATABASE newsagent;

-- select database
USE newsagent;

-- create tables
DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
  account_id    INT AUTO_INCREMENT,
  username   	VARCHAR(30),
  pass			VARCHAR(30),
  PRIMARY KEY (account_id)
);


DROP TABLE IF EXISTS delivery_person;
CREATE TABLE delivery_person
(
	delivery_person_id 	INT AUTO_INCREMENT NOT NULL,
	firstName   		VARCHAR(30) NOT NULL,
	lastName    		VARCHAR(30) NOT NULL,
    phonenumber		 	VARCHAR(10) NOT NULL,
    deliveryArea 		VARCHAR(3) NOT NULL,
    PRIMARY KEY(delivery_person_id)

);


DROP TABLE IF EXISTS delivery_area;
CREATE TABLE delivery_area
(
	delivery_area_id 		INT AUTO_INCREMENT NOT NULL,
    area_name 				VARCHAR(3) NOT NULL,
    routingkey 				VARCHAR(3) 	NOT NULL,
    primary key(delivery_area_id)
);

INSERT INTO delivery_area VALUES(1, "WEM", "N91");




DROP TABLE IF EXISTS customers;
CREATE TABLE customers
(
  customer_id       INT AUTO_INCREMENT NOT NULL,
  firstName   		VARCHAR(30) NOT NULL,
  lastName    		VARCHAR(30) NOT NULL,
  phone     		VARCHAR(10) NOT NULL,
  custEircode      	VARCHAR(10) NOT NULL,
  age     			INT  NOT NULL,
  deliveryArea 		Varchar(3) NOT NULL,
  atHomeStatus 		Varchar(10) NOT NULL,
	PRIMARY KEY (customer_id)
	
);

DROP TABLE IF EXISTS customersArchive;
CREATE TABLE customersArchive
(
  customer_id_Archive       INT NOT NULL,
  firstName_Archive    		VARCHAR(30) NOT NULL,
  lastName_Archive     		VARCHAR(30) NOT NULL,
  phone_Archive      		VARCHAR(10) NOT NULL,
  custEircode_Archive       VARCHAR(10) NOT NULL,
  age_Archive      			INT  NOT NULL,
  deliveryArea_Archive  	Varchar(3) NOT NULL,
  atHomeStatus_Archive  	Varchar(10) NOT NULL,
	PRIMARY KEY (customer_id_Archive )
	
);

DROP TABLE IF EXISTS publications;
CREATE TABLE publications 
(
	publication_id 		INT AUTO_INCREMENT NOT NULL,
    pub_name 			VARCHAR(50) NOT NULL,
    price 				DOUBLE NOT NULL,
    pub_description 	VARCHAR(255) NOT NULL,
	frequency 			VARCHAR(20) NOT NULL,
    primary key (publication_id)
);


DROP TABLE IF EXISTS invoices;
CREATE TABLE invoices 
(
	invoice_id  		INT AUTO_INCREMENT NOT NULL,
	customerName 		VARCHAR(50) NOT NULL,
	customerSurname		VARCHAR(50) NOT NULL,
	customerEircode		VARCHAR(10) NOT NULL,
    companyName 		VARCHAR(50) NOT NULL,
    companyEircode 		VARCHAR(10) NOT NULL,
    publicationName 	VARCHAR(50) NOT NULL,
    price 				DOUBLE NOT NULL,
    currentDate 		VARCHAR(20),
    PRIMARY KEY (invoice_id)
);

DROP TABLE IF EXISTS delivery_dockets;
CREATE TABLE delivery_dockets
(
	delivery_docket_id 	INT AUTO_INCREMENT NOT NULL,
	firstName			VARCHAR(50) NOT NULL,
	lastName			VARCHAR(50) NOT NULL,
	eircode				VARCHAR(10) NOT NULL,
	itemName			VARCHAR(50) NOT NULL,
	itemDescription		VARCHAR(255) NOT NULL,
    primary key (delivery_docket_id)
);


DROP TABLE IF EXISTS subscriptions;
CREATE TABLE subscriptions 
(
	subscription_id 	INT AUTO_INCREMENT NOT NULL,
    frequency 			VARCHAR(20) NOT NULL,
    quantity 			INT NOT NULL,
	price 				DOUBLE NOT NULL,
    customer_id 		INT NOT NULL,
    publication_id 		INT NOT NULL,
    primary key (subscription_id),
	FOREIGN KEY (customer_id) REFERENCES customers (customer_id),
	FOREIGN KEY (publication_id) REFERENCES publications (publication_id)
);


#updateCustomerData stored procedure
DROP PROCEDURE IF EXISTS updateCustomerData;
DELIMITER //
CREATE PROCEDURE updateCustomerData
(
IN	customerID_para 	INTEGER,
IN	firstName_para   	VARCHAR(30),
IN	lastName_para    	VARCHAR(30),
IN	phone_para     		VARCHAR(10),
IN	custEircode_para    VARCHAR(10),
IN	age_para     		INT,
IN	deliveryArea_para 	Varchar(3)
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.customers
	SET firstName =	firstName_para,
  	lastName = lastName_para,   		
  	phone =  phone_para, 		
  	custEircode = custEircode_para,  		
  	age = age_para, 			
  	deliveryArea = deliveryArea_para
	
	WHERE customer_id = customerID_para;
	
END//


#updatePubData stored procedure
DROP PROCEDURE IF EXISTS updatePublicationData;
DELIMITER //
CREATE PROCEDURE updatePublicationData
(
IN	publication_id_para 	INTEGER,
IN	pub_name_para   		VARCHAR(50),
IN	price_para    			DOUBLE,
IN	pub_description_para	VARCHAR(255),
IN	frequency_para   		VARCHAR(20)
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.publications
	SET pub_name  =	pub_name_para,
  	price  = price_para,   			
  	pub_description  = pub_description_para,  		
  	frequency  = frequency_para
	
	WHERE publication_id  = publication_id_para;
	
END//


#updateInvoice Data stored procedure
DROP PROCEDURE IF EXISTS updateInvoiceData;
DELIMITER //
CREATE PROCEDURE updateInvoiceData
(
IN	invoice_id_para 			INT,
IN	customerName_para 			VARCHAR(50),
IN	customerSurname_para		VARCHAR(50),
IN	customerEircode_para		VARCHAR(10),
IN  companyName_para 			VARCHAR(50),
IN  companyEircode_para 		VARCHAR(10),
IN  publicationName_para 		VARCHAR(50),
IN  price_para 					DOUBLE,
IN  currentDate_para 			VARCHAR(20)
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.invoices
	SET customerName = customerName_para,
	customerSurname	= customerSurname_para,	
	customerEircode	= customerEircode_para,	
    companyName = companyName_para, 		
    companyEircode 	= companyEircode_para,	
    publicationName = publicationName_para,	
    price = price_para,				
    currentDate = currentDate_para		
	
	WHERE invoice_id = invoice_id_para;
	
END//



#updateDeliveryArea Data stored procedure
DROP PROCEDURE IF EXISTS updateDeliveryAreaData;
DELIMITER //
CREATE PROCEDURE updateDeliveryAreaData
(
IN	delivery_area_id_para 	INT,
IN	area_name_para 			VARCHAR(3),
IN	routingkey_para			VARCHAR(3)
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.delivery_area
	SET area_name  = area_name_para,
	routingkey 	= routingkey_para		
    
	WHERE delivery_area_id  = delivery_area_id_para;
	
END//


#updateDeliveryDocketData stored procedure
DROP PROCEDURE IF EXISTS updateDeliveryDocketData;
DELIMITER //
CREATE PROCEDURE updateDeliveryDocketData
(
IN	delivery_docket_id_para 	INT,
IN	firstName_para				VARCHAR(50),
IN	lastName_para				VARCHAR(50),
IN	eircode_para				VARCHAR(10),
IN	itemName_para				VARCHAR(50),
IN	itemDescription_para		VARCHAR(255)
)
BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.delivery_dockets
	SET firstName =	firstName_para,
	lastName =	lastName_para,	
	eircode	=	eircode_para,		
	itemName =	itemName_para,	
	itemDescription	=	itemDescription_para
	WHERE delivery_docket_id  = delivery_docket_id_para;
	
END//


#updateDeliveryPerson Data stored procedure
DROP PROCEDURE IF EXISTS updateDeliveryPerson;
DELIMITER //
CREATE PROCEDURE updateDeliveryPerson
(
IN	delivery_person_id_para 	INT,
IN	firstName_para   		VARCHAR(30),
IN	lastName_para    		VARCHAR(30),
IN  phonenumber_para		 VARCHAR(10),
IN  deliveryArea_para 		VARCHAR(3) 
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;  
UPDATE newsagent.delivery_person
	SET firstName  = firstName_para,
	lastName = routingkey_para,	
	phonenumber =phonenumber_para,
    deliveryArea = deliveryArea_para
	WHERE delivery_person_id   = delivery_person_id_para;
	
END//


#updateAccountDetails Data stored procedure
DROP PROCEDURE IF EXISTS updateAccountDetails;
DELIMITER //
CREATE PROCEDURE updateAccountDetails
(
  account_id_para    INT,
  username_para   	VARCHAR(50),
  pass_para			VARCHAR(50)
)

BEGIN
  DECLARE sql_error TINYINT DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SET sql_error = TRUE;

	START TRANSACTION;
UPDATE newsagent.delivery_area 
SET 
    account_id = account_id_para,
    username = username_para,
    pass = pass_para
WHERE
    delivery_area_id = delivery_area_id_para;
	
END//