import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {

	private Connection connect = null;
	private Statement statement = null;
	private CallableStatement callableStatement = null;
	private PreparedStatement preparedStatement = null;
	private PreparedStatement preparedStatement2 = null;
	private ResultSet resultSet = null;

	final private String host = "localhost:3306";
	final private String user = "root";
	final private String password = "admin";
	final private String url = "jdbc:mysql://localhost:3306/newsagent?serverTimezone=UTC";

	// loads the driver and sets up the connection with DB
	public Database() throws Exception {

		try {
			// Load MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			// Setup the connection with the DB
			connect = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw e;
		}
	}

	// closes the connection | null for testing purposes | testDatabase002
	public String setConnection() throws SQLException {
		connect.close();
		return null;
	}

	// create an account
	public boolean insertAccountDetails(Account a) throws AccountExceptionHandler {
		boolean insertSucessfull = true;
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT EXISTS(SELECT 1 FROM newsagent.accounts WHERE username = '"+ a.getUsername() + "' AND pass = '" + a.getPassword() + "') AS 'Accounts';");
			resultSet.next();
			int exists = resultSet.getInt("Accounts");

			if (exists == 1) {
				throw new AccountExceptionHandler("Account with that name already exists");
			} else {
				// Create prepared statement to issue SQL query to the database
				preparedStatement = connect.prepareStatement("insert into newsagent.accounts values (default, ?, ?)");
				preparedStatement.setString(1, a.getUsername());
				preparedStatement.setString(2, a.getPassword());
				preparedStatement.executeUpdate();
			}
		} catch (Exception e) {
			System.err.println("Error with  insertAccountDetails:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertAccountDetails

	// modify an account
	public boolean modifyAccount(int accountID, String username, String password) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateDeliveryAreaData(?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, accountID);
			callableStatement.setString(2, username);
			callableStatement.setString(3, password);
			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyAccount:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	// view an account
	public boolean verifyAccount(String username, String password) {
		boolean viewSuccessfull = false;

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT EXISTS(SELECT 1 FROM newsagent.accounts WHERE username = '"
					+ username + "' AND pass = '" + password + "') AS 'Accounts';");
			resultSet.next();
			int exists = resultSet.getInt("Accounts");

			if (exists == 1) {
				viewSuccessfull = true;
			}

		} catch (Exception e) {
			System.err.println("Error with viewOneAccount :\n" + e.toString());
		}

		return viewSuccessfull;
	}

//	// view all accounts
//	public ResultSet viewAllAccounts() {
//		try {
//			statement = connect.createStatement();
//			resultSet = statement.executeQuery("Select * from newsagent.accounts");
//
//			// using metadata to get all data from table
//			ResultSetMetaData rsmd = (ResultSetMetaData) resultSet.getMetaData();
//			int columnsNumber = rsmd.getColumnCount();
//
//			while (resultSet.next()) {
//				// Print one row
//				for (int i = 1; i <= columnsNumber; i++) {
//					System.out.print(resultSet.getString(i) + " "); // Print one element of a row
//				}
//				System.out.println();// Move to the next line to print the next row.
//			}
//		} catch (Exception e) {
//			System.err.println("Error with viewAllAccounts :\n" + e.toString());
//			resultSet = null;
//		}
//		return resultSet;
//	}
//
//	// delete account
//	public boolean deleteAccountById(int accountID) {
//		boolean deleteSucessfull = true;
//		// Add Code here to call embedded SQL to insert Customer into DB
//		try {
//			// Create prepared statement to issue SQL query to the database
//			if (accountID == -99)
//				// Delete all entries in Table
//				preparedStatement = connect.prepareStatement("delete from newsagent.accounts");
//			else
//				// Delete a particular account
//				preparedStatement = connect
//						.prepareStatement("delete from newsagent.accounts where account_id  = " + accountID);
//			preparedStatement.executeUpdate();
//		} catch (Exception e) {
//			System.err.println("Error with deleteAccountById :\n" + e.toString());
//			deleteSucessfull = false;
//		}
//		return deleteSucessfull;
//	}

	/*********************************************************************************************************/
	// create a customer
	public boolean insertCustomerDetails(Customer c) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect
					.prepareStatement("insert into newsagent.customers values (default, ?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, c.getName());
			preparedStatement.setString(2, c.getSurname());
			preparedStatement.setString(3, c.getPhoneNumber());
			preparedStatement.setString(4, c.getEircode());
			preparedStatement.setInt(5, c.getAge());
			preparedStatement.setString(6, c.getDeliveryArea());
			preparedStatement.setString(7, c.getAtHomeStatus());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertCustomerDetails

	// modify a customer
	public boolean modifyCustomerById(int custID, String custName, String custSurname, String custNum,
			String custEircode, int custAge, String delArea) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateCustomerData(?, ?, ?, ?, ?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, custID);
			callableStatement.setString(2, custName);
			callableStatement.setString(3, custSurname);
			callableStatement.setString(4, custNum);
			callableStatement.setString(5, custEircode);
			callableStatement.setInt(6, custAge);
			callableStatement.setString(7, delArea);
			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyCustomerById:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end modifyCustomerById

	public boolean modifyCustomerStatusById(int custID, String status) {
		boolean insertSucessfull = true;
		try {
			String SQL = "UPDATE newsagent.customers SET atHomeStatus = ? WHERE customer_id = ?";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setString(1, status);
			callableStatement.setInt(2, custID);
			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyCustomerStatusById:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	// view one customer
	public boolean viewOneCustomer(int custID) {
		boolean viewSuccessfull = true;

		// Add Code here to call embedded SQL to view Customer Details
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.customers where customer_id = " + custID);
		} catch (Exception e) {
			System.err.println("Error with viewOneCustomer :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}

	// view all customers
	public ResultSet viewAllCustomers() {

		// Add Code here to call embedded SQL to view Customer Details
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.customers");
		} catch (Exception e) {
			System.err.println("Error with viewAllCustomers :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	}

	// delete customer
	public boolean deleteCustomerById(int custID) {
		boolean deleteSucessfull = true;
		// Add Code here to call embedded SQL to insert Customer into DB
		try {
			// Create prepared statement to issue SQL query to the database
			if (custID == -99)
				// Delete all entries in customers and insert into customer
				preparedStatement = connect.prepareStatement("delete from newsagent.customers");
			else {
				// Delete a particular Customer
				preparedStatement = connect
						.prepareStatement("delete from newsagent.customers where customer_id = " + custID);
			preparedStatement.executeUpdate();
			}
		} catch (Exception e) {
			System.err.println("Error with deleteCustomerById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}

	
	//archive customer
	public boolean archiveCustomerByID(int custID) {
		boolean archiveSuccessfulll = true;
		try {
			if (custID == -99) {
				preparedStatement = connect.prepareStatement("INSERT INTO newsagent.customersArchive(customer_id_Archive, firstName_Archive "
						+ " lastName_Archive, phone_Archive, custEircode_Archive, age_Archive, deliveryArea_Archive, atHomeStatus_Archive) "
						+ " SELECT customer_id, firstName, lastName, phone, custEircode, age, deliveryArea, atHomeStatus FROM newsagent.customers");
				preparedStatement.executeUpdate();
				
				preparedStatement2 = connect.prepareStatement("DELETE FROM newsagent.customers");
				preparedStatement2.executeUpdate();

			}else {
				preparedStatement = connect.prepareStatement("INSERT INTO newsagent.customersArchive(customer_id_Archive, firstName_Archive "
						+ " lastName_Archive, phone_Archive, custEircode_Archive, age_Archive, deliveryArea_Archive, atHomeStatus_Archive) "
						+ " SELECT customer_id, firstName, lastName, phone, custEircode, age, deliveryArea, atHomeStatus FROM newsagent.customers"
						+ " WHERE customer_id = " + custID);
				preparedStatement.executeUpdate();
				
				preparedStatement2 = connect.prepareStatement("delete from newsagent.customers where customer_id = " + custID);
				preparedStatement2.executeUpdate();
			}
		} catch (Exception e) {
			System.err.println("Error with archiveCustomerByID :\n" + e.toString());
			archiveSuccessfulll = false;
		}
		return archiveSuccessfulll;
	}
	/*********************************************************************************************************/
	// insert publication details
	public boolean insertPublication(Publication p) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect
					.prepareStatement("insert into newsagent.publications values (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, p.getPublicationName());
			preparedStatement.setDouble(2, p.getPublicationPrice());
			preparedStatement.setString(3, p.getPublicationDescription());
			preparedStatement.setString(4, p.getPublicationFrequency());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with insertPublication :\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertPublication

	// modify a publication
	public boolean modifyPublicationByID(int pubID, String publicationName, double publicationPrice,
			String publicationDescription, String publicationFrequency) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updatePublicationData(?, ?, ?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, pubID);
			callableStatement.setString(2, publicationName);
			callableStatement.setDouble(3, publicationPrice);
			callableStatement.setString(4, publicationDescription);
			callableStatement.setString(5, publicationFrequency);

			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyPublicationByID:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end modifyPublicationByID

	// view one publication by Id
	public boolean viewOnePublication(int pubID) {
		boolean viewSuccessfull = true;

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.publications where publication_id = " + pubID);
		} catch (Exception e) {
			System.err.println("Error with viewOnePublication :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}// end of viewOnePublication

	// viewAllPublications
	public ResultSet viewAllPublications() {
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.publications");
		} catch (Exception e) {
			System.err.println("Error with viewAllPublications :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	} // end of viewAllPublications

	// deletePublicationrById
	public boolean deletePublicationrById(int pubID) {
		boolean deleteSucessfull = true;
		// Add Code here to call embedded SQL to insert Customer into DB
		try {
			// Create prepared statement to issue SQL query to the database
			if (pubID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.publications");
			else
				// Delete a particular publication
				preparedStatement = connect
						.prepareStatement("delete from newsagent.publications where publication_id = " + pubID);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with deletePublicationrById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}// end of deletePublicationrById

	/*********************************************************************************************************/
	// insert invoice details
	public boolean insertInvoice(Invoice inv) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect
					.prepareStatement("insert into newsagent.invoices values (default, ?, ?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, inv.getCustomerFirstName());
			preparedStatement.setString(2, inv.getCustomerLastName());
			preparedStatement.setString(3, inv.getCustomerEircode());
			preparedStatement.setString(4, inv.getCompanyName());
			preparedStatement.setString(5, inv.getCompanyEircode());
			preparedStatement.setString(6, inv.getItemName());
			preparedStatement.setDouble(7, inv.getTotalPrice());
			preparedStatement.setString(8, inv.getStringDate());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with insertInvoice :\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertInvoice

	// modify an invoice
	public boolean modifyInvoiceByID(int invID, String custName, String custSurname, String custEircode,
			String compName, String compEircode, String pubName, double totalPrice, String currentDate) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateInvoiceData(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, invID);
			callableStatement.setString(2, custName);
			callableStatement.setString(3, custSurname);
			callableStatement.setString(4, custEircode);
			callableStatement.setString(5, compName);
			callableStatement.setString(6, compEircode);
			callableStatement.setString(7, pubName);
			callableStatement.setDouble(8, totalPrice);
			callableStatement.setString(9, currentDate);

			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyInvoiceByID:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}//

	// view one invoice
	public boolean viewOneInvoice(int invID) {
		boolean viewSuccessfull = true;

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.invoices where invoice_id = " + invID);
		} catch (Exception e) {
			System.err.println("Error with viewOneInvoice :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}

	// view all invoices
	public ResultSet viewAllInvoices() {

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.invoices");

			// using metadata to get all data from table
			ResultSetMetaData rsmd = (ResultSetMetaData) resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();

			while (resultSet.next()) {
				// Print one row
				for (int i = 1; i <= columnsNumber; i++) {
					System.out.print(resultSet.getString(i) + " "); // Print one element of a row
				}
				System.out.println();// Move to the next line to print the next row.
			}
		} catch (Exception e) {
			System.err.println("Error with viewAllInvoices :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	}

	// delete invoice
	public boolean deleteInvoiceById(int invID) {
		boolean deleteSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			if (invID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.invoices");
			else
				// Delete a particular invoice
				preparedStatement = connect
						.prepareStatement("delete from newsagent.invoices where invoice_id   = " + invID);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with deleteInvoiceById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}

	/*********************************************************************************************************/
	// insert delivery area details
	public boolean insertDeliveryArea(Deliveryarea del) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into newsagent.delivery_area values (default, ?, ?)");
			preparedStatement.setString(1, del.getareaname());
			preparedStatement.setString(2, del.getroutingkey());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with insertDeliveryArea :\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertDeliveryArea

	// modify a delivery area
	public boolean modifyDeliveryArea(int delAreaID, String delAreaName, String routingKey) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateDeliveryAreaData(?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, delAreaID);
			callableStatement.setString(2, delAreaName);
			callableStatement.setString(3, routingKey);

			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyDeliveryArea:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	// view one delivery area
	public boolean viewOneDeliveryArea(int delAreaID) {
		boolean viewSuccessfull = true;
		try {
			statement = connect.createStatement();
			resultSet = statement
					.executeQuery("Select * from newsagent.delivery_area where delivery_area_id = " + delAreaID);
		} catch (Exception e) {
			System.err.println("Error with viewOneDeliveryArea :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}

	// check del area exists
	public boolean checkDelAreaExists(String delAreaName) {
		boolean viewSuccessfull = false;

		try {
			statement = connect.createStatement();
			resultSet = statement
					.executeQuery("Select delAreaName from newsagent.delivery_area where area_name = " + delAreaName);
			viewSuccessfull = true;
		} catch (Exception e) {
			System.err.println("Error with checkDelAreaExists :\n" + e.toString());

		}
		return viewSuccessfull;
	}

	// viewAllDeliveryAreas
	public ResultSet viewAllDeliveryAreas() {

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.delivery_area;");
		} catch (Exception e) {
			System.err.println("Error with viewAllDeliveryAreas :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	}

	// delete delivery area
	public boolean deleteDeliveryArearById(int delAreaID) {
		boolean deleteSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			if (delAreaID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.delivery_area");
			else
				// Delete a particular delivery area
				preparedStatement = connect
						.prepareStatement("delete from newsagent.delivery_area where delivery_area_id  = " + delAreaID);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with deleteDeliveryArearById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}

	/*********************************************************************************************************/
	// insert deliveryPerson details
	public boolean insertDeliveryPerson(Deliveryperson delPerson) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect
					.prepareStatement("insert into newsagent.delivery_area values (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, delPerson.getFirstName());
			preparedStatement.setString(2, delPerson.getLastName());
			preparedStatement.setString(3, delPerson.getPhoneNumber());
			preparedStatement.setString(4, delPerson.getDeliveryArea());
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with insertDeliveryPerson :\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertDeliveryPerson

	// modify a deliveryPerson
	public boolean modifyDeliveryPerson(int delPersonID, String delFname, String delSname, String delNum,
			String delArea) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateDeliveryAreaData(?, ?, ?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, delPersonID);
			callableStatement.setString(2, delFname);
			callableStatement.setString(3, delSname);
			callableStatement.setString(4, delNum);
			callableStatement.setString(5, delArea);

			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyDeliveryPerson:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	// view one deliveryPerson
	public boolean viewOneDeliveryPerson(int delPersonID) {
		boolean viewSuccessfull = true;

		try {
			statement = connect.createStatement();
			resultSet = statement
					.executeQuery("Select * from newsagent.delivery_person where delivery_person_id = " + delPersonID);
		} catch (Exception e) {
			System.err.println("Error with viewOneDeliveryPerson :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}

	// view all deliveryPerson
	public ResultSet viewAllDeliveryPerson() {

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.delivery_person");
		} catch (Exception e) {
			System.err.println("Error with viewAllDeliveryPerson :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	}

	// delete deliveryPerson
	public boolean deleteDeliveryPersonById(int delPersonID) {
		boolean deleteSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			if (delPersonID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.delivery_person");
			else
				// Delete a particular deliveryPerson
				preparedStatement = connect.prepareStatement(
						"delete from newsagent.delivery_person where delivery_person_id = " + delPersonID);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with deleteDeliveryPersonById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}

	/*********************************************************************************************************/
	// insert Delivery docket details
	public boolean insertDeliveryDocket(Deliverydocket d) {
		boolean insertSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			preparedStatement = connect
					.prepareStatement("insert into newsagent.delivery_dockets values (default, ?, ?, ?, ?, ?)");
			preparedStatement.setString(1, d.getfirstname());
			preparedStatement.setString(2, d.getlastname());
			preparedStatement.setString(3, d.geteircode());
			preparedStatement.setString(4, d.getitemname());
			preparedStatement.setString(5, d.getitemdescription());

			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with insertDeliveryDocket :\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}// end insertDeliveryDocket

	// modify a deliveryDocket
	public boolean modifyDeliveryDocket(int delDocketID, String fName, String lastName, String eircode, String itemName,
			String itemDescription) {
		boolean insertSucessfull = true;
		try {
			String SQL = "{call updateDeliveryDocketData(?, ?, ?, ?, ?, ?)}";
			callableStatement = connect.prepareCall(SQL);

			callableStatement.setInt(1, delDocketID);
			callableStatement.setString(2, fName);
			callableStatement.setString(3, lastName);
			callableStatement.setString(4, eircode);
			callableStatement.setString(5, itemName);
			callableStatement.setString(6, itemDescription);

			callableStatement.execute();
		} catch (Exception e) {
			System.err.println("Error with  modifyDeliveryDocket:\n" + e.toString());
			insertSucessfull = false;
		}
		return insertSucessfull;
	}

	// view one deliverydocket
	public boolean viewOneDeliveryDocket(int delDocketID) {
		boolean viewSuccessfull = true;

		try {
			statement = connect.createStatement();
			resultSet = statement
					.executeQuery("Select * from newsagent.delivery_dockets where delivery_docket_id = " + delDocketID);
		} catch (Exception e) {
			System.err.println("Error with viewOneDeliveryDocket :\n" + e.toString());
			viewSuccessfull = false;
		}
		return viewSuccessfull;
	}

	// view all deliverydockets
	public ResultSet viewAllDeliveryDocket() {

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.delivery_dockets");
		} catch (Exception e) {
			System.err.println("Error with viewAllDeliveryDocket :\n" + e.toString());
			resultSet = null;
		}
		return resultSet;
	}

	// delete deliveryDocket
	public boolean deleteDeliveryDocketById(int delDocketID) {
		boolean deleteSucessfull = true;
		try {
			// Create prepared statement to issue SQL query to the database
			if (delDocketID == -99)
				// Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.delivery_dockets");
			else
				// Delete a particular deliveryDocket
				preparedStatement = connect.prepareStatement(
						"delete from newsagent.delivery_dockets where delivery_docket_id  = " + delDocketID);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println("Error with deleteDeliveryDocketById :\n" + e.toString());
			deleteSucessfull = false;
		}
		return deleteSucessfull;
	}
}