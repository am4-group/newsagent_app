
public class Deliveryperson {
	
	private String phonenumber;
	private String firstname;
	private String lastname;
	private String deliveryarea;
	
	public Deliveryperson(String firstname, String lastname, String phonenumber, String deliveryarea)throws DeliverypersonExceptionHandler{
		try {
			validatefirstname(firstname);
			validatelastname(lastname);
			validatephonenumber(phonenumber);
			validateDeliveryArea(deliveryarea);
		}
		
		catch (DeliverypersonExceptionHandler e) {
			throw e;
			
		}
		this.firstname = firstname;
		this.lastname = lastname;
		this.phonenumber = phonenumber;
		this.deliveryarea = deliveryarea;
		}
	
	public static String validatefirstname(String firstname) throws DeliverypersonExceptionHandler{
		
		if (firstname.isBlank() || firstname.isEmpty())
			throw new DeliverypersonExceptionHandler("Delivery Person name not entered");
		else if (firstname.length() < 2)
			throw new DeliverypersonExceptionHandler("Delivery Person name does not meet minimum length requirements");
		else if (firstname.length() > 30)
			throw new DeliverypersonExceptionHandler("Delivery Person name exceeds maximum length requirements");
		else 
			return "Delivery Person name valid";
	}
	
	public static String validatelastname(String lastname) throws DeliverypersonExceptionHandler{
		if (lastname.isBlank() || lastname.isEmpty())
			throw new DeliverypersonExceptionHandler("Delivery Person Last name not entered");
		else if (lastname.length() < 2)
			throw new DeliverypersonExceptionHandler("Delivery Person Last name does not meet minimum length requirements");
		else if (lastname.length() > 30)
			throw new DeliverypersonExceptionHandler("Delivery Person Last name exceeds maximum length requirements");
		else 
			return "Delivery Person Last name valid";
	}
	
	public static String validatephonenumber(String phonenumber) throws DeliverypersonExceptionHandler{
		if (phonenumber.isBlank() ||phonenumber.isEmpty())
			throw new DeliverypersonExceptionHandler("Delivery Person phone number not entered");
		else if (phonenumber.length() < 10)
			throw new DeliverypersonExceptionHandler("Delivery Person phone number does not meet minimum length requirements");
		else if (phonenumber.length() > 10)
			throw new DeliverypersonExceptionHandler("Delivery Person phone number exceeds maximum length requirements");
		else 
			return "Delivery Person phone number is valid";
	}
	public static String validateDeliveryArea(String deliveryarea) throws DeliverypersonExceptionHandler {
		if (deliveryarea.isBlank() ||deliveryarea.isEmpty())
			throw new DeliverypersonExceptionHandler("Delivery area not entered");
		else if (deliveryarea.length()< 3)
			throw new DeliverypersonExceptionHandler("Delivery area does not meet minimum length requirements");
		else if(deliveryarea.length()> 3)
			throw new DeliverypersonExceptionHandler("Delivery area exceeds maximum length requirements");
		else
			return "Delivery area valid";
	}
	
	
	public String getFirstName() {
		return firstname;
	}
	
	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastName() {
		return lastname;
	}
	
	public void setLastName(String lastname) {
		this.lastname = lastname;
	}
	
	public String getPhoneNumber() {
		return phonenumber;
	}
	
	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getDeliveryArea() {
		return deliveryarea;
	}

	public void setDeliveryArea(String deliveryarea) {
		this.deliveryarea = deliveryarea;
	}

}
