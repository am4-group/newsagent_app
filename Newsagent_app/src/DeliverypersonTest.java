import junit.framework.TestCase;

public class DeliverypersonTest extends TestCase {

	// Test No: 1
	// Test Objective: Verify Valid input will create New Delivery Person
	// Test Type JUnit
	// Input(s) First name: "Co"
	// Last name: "Ke"
	// Phone number: "0833424344"
	// Delivery Area: "WEM"
	// Expected Output "Delivery Person Created"

	public void testvalidateDeliveryperson001() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson testObj = new Deliveryperson("Co", "Ke", "0833424344", "WEM");
			assertEquals("Co", testObj.getFirstName());
			assertEquals("Ke", testObj.getLastName());
			assertEquals("0833424344", testObj.getPhoneNumber());
			assertEquals("WEM", testObj.getDeliveryArea());
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 2
	// Test Objective Verify Empty Inputs return Invalid
	// Test Type JUnit
	// Input(s) First name: ""
	// Last name: ""
	// Phone number: ""
	// Delivery Area ""
	// Expected Output "Please enter valid values"

	public void testvalidateDeliveryperson002() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson testObj = new Deliveryperson("", "", "", "");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name not entered", e.getMessage());
		}
	}

	// Test No: 3
	// Test Objective to test delivery person attributes with a single input as
	// invalid
	// Test Type JUnit
	// Input(s) First name: "C"
	// Last name: "K"
	// Phone number: "0"
	// Delivery Area "W"
	// Expected Output "Delivery Person Details are invalid"

	public void testvalidateDeliveryperson003() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson testObj = new Deliveryperson("C", "K", "0", "W");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 4
	// Test Objective to test delivery person First name, Last name, and phone
	// number with invalid inputs
	// Test Type JUnit
	// Input(s) First name:"CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
	// Last name:"KKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Phone Number:"083342424"
	// Delivery Area "WE"
	// Expected Output "Invalid Details Entered"

	public void testvalidateDeliveryperson004() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson testObj = new Deliveryperson("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC",
					"KKKKKKKKKKKKKKKKKKKKKKKKKKKKK", "083342424", "WE");
			fail("Exception Expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 5
	// Test Objective to test delivery person First Name, Last Name and Phone Number
	// with valid and invalid inputs
	// Test Type JUnit
	// Input(s) First name: "CCCCCCCCCCCCCCCCCCCCC"
	// Last name: "CCCCCCCCCCCCCCCCCCCCC"
	// Phone Number: "08334242444"
	// Delivery Area "WEMA"
	// Expected Output: "Invalid Details entered"

	public void testvalidateDeliveryperson005() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson testObj = new Deliveryperson("CCCCCCCCCCCCCCCCCCCCC", "CCCCCCCCCCCCCCCCCCCCC", "08334242444",
					"WEMA");
			fail("Exception Not Expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number exceeds maximum length requirements", e.getMessage());
		}
	}

	// ==================================================================================================================
	// Test No: 6
	// Test Objective Verify no user input will return "Delivery Person First name
	// not entered"
	// Test Type JUnit
	// Input(s) ""
	// Expected Output: "Delivery Person First name not entered"

	public void testvalidateFirstName001() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatefirstname("");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name not entered", e.getMessage());
		}
	}

	// Test No: 7
	// Test Objective To test a valid delivery person first name entered with 1
	// value is too Short
	// Test Type JUnit
	// Input(s) "C"
	// Expected Output: "Delivery Person First name entered is too short"

	public void testvalidateFirstName002() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatefirstname("C");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 8
	// Test Objective: To test a valid delivery person first name entered with 2
	// value is accepted
	// Test Type JUnit
	// Input(s) "Co"
	// Expected Output:"Delivery Person First name valid"

	public void testvalidateFirstName003() throws DeliverypersonExceptionHandler {

		try {
			String result = Deliveryperson.validatefirstname("Co");
			assertEquals("Delivery Person name valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 9
	// Test Objective To test a valid delivery person first name entered with 30
	// value is accepted
	// Test Type JUnit
	// Input(s) "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
	// Expected Output:"Delivery Person First name valid"

	public void testvalidateFirstName004() throws DeliverypersonExceptionHandler {
		try {
			String result = Deliveryperson.validatefirstname("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
			assertEquals("Delivery Person name valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 10
	// Test Objective To test a valid delivery person first name entered with 31
	// value is invalid
	// Test Type JUnit
	// Input(s) "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
	// Expected Output "Delivery Person First name entered is too long"

	public void testvalidateFirstName005() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validatefirstname("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No: 11
	// Test Objective To test a valid delivery person first name entered with 40
	// value is invalid
	// Test Type JUnit
	// Input(s) "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
	// Expected Output "Delivery Person First name entered is too long"

	public void testvalidateFirstName006() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validatefirstname("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person name exceeds maximum length requirements", e.getMessage());
		}
	}
//========================================================================================================================

	// Test No: 12
	// Test Objective Verify no user input will return "Delivery Person Last name
	// not entered"
	// Test Type JUnit
	// Input(s) ""
	// Expected Output: "Delivery Person Last name not entered"

	public void testvalidateLastName001() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatelastname("");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person Last name not entered", e.getMessage());
		}
	}

	// Test No: 13
	// Test Objective To test a valid delivery person Last name entered with 1 value
	// is too Short
	// Test Type JUnit
	// Input(s) "K"
	// Expected Output: "Delivery Person Last name entered is too short"

	public void testvalidateLastName002() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatelastname("K");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person Last name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 14
	// Test Objective: To test a valid delivery person Last name entered with 2
	// value is accepted
	// Test Type JUnit
	// Input(s) "Ke"
	// Expected Output:"Delivery Person First name valid"

	public void testvalidateLastName003() throws DeliverypersonExceptionHandler {

		try {
			String result = Deliveryperson.validatelastname("Ke");
			assertEquals("Delivery Person Last name valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 15
	// Test Objective To test a valid delivery person Last name entered with 30
	// value is accepted
	// Test Type JUnit
	// Input(s) "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output:"Delivery Person Last name valid"

	public void testvalidateLastName004() throws DeliverypersonExceptionHandler {
		try {
			String result = Deliveryperson.validatelastname("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			assertEquals("Delivery Person Last name valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 16
	// Test Objective To test a valid delivery person Last name entered with 31
	// value is invalid
	// Test Type JUnit
	// Input(s) "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output:"Delivery Person Last name is too long"

	public void testvalidateLastName005() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validatelastname("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person Last name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No: 17
	// Test Objective To test a valid delivery person Last name entered with 40
	// value is invalid
	// Test Type JUnit
	// Input(s) "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
	// Expected Output:"Delivery Person Last name is too long"

	public void testvalidateLastName006() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validatelastname("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person Last name exceeds maximum length requirements", e.getMessage());
		}
	}
//======================================================================================================================================

	// Test No: 18
	// Objective: To test no phone number entered is invalid
	// Test Type JUnit
	// Input(s): ""
	// Expected Output: Delivery Person phone Number not entered

	public void testvalidatePhoneNumber001() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatephonenumber("");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number not entered", e.getMessage());
		}
	}

	// Test No: 19
	// Objective: to test one number entered is invalid
	// Test Type JUnit
	// Input(s): ""
	// Expected Output: Delivery Person phone Number not entered
	public void testvalidatePhoneNumber002() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatephonenumber("0");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 20
	// Objective: To test an input of 9 is invalid
	// Test Type JUnit
	// Input(s): "083342434"
	// Expected Output: "Delivery Person phone Number entered is too short"

	public void testvalidatePhoneNumber004() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatephonenumber("083342434");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 21
	// Objective: To test an input of 10 is valid
	// Test Type JUnit
	// Input(s): "0833424344"
	// Expected Output: "Delivery Person phone Number entered"

	public void testvalidatePhoneNumber005() throws DeliverypersonExceptionHandler {

		try {
			String result = Deliveryperson.validatephonenumber("0833424344");
			assertEquals("Delivery Person phone number is valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 22
	// Objective: To test an input of 11 is invalid
	// Test Type JUnit
	// Input(s): "08334243444"
	// Expected Output: "Delivery Person phone number exceeds maximum length requirements"

	public void testvalidatePhoneNumber006() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatephonenumber("08334243444");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No: 23
	// Objective: To test an input of 20 is invalid
	// Test Type JUnit
	// Input(s): "08334243444444444444"
	// Expected Output: "Delivery Person phone Number entered is too short"

	public void testvalidatePhoneNumber007() throws DeliverypersonExceptionHandler {

		try {
			Deliveryperson.validatephonenumber("08334243444444444444");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery Person phone number exceeds maximum length requirements", e.getMessage());
		}
	}
//===========================================================================================================================

	// Test No: 24
	// Objective: Verify no input delivery area is invalid
	// Test Type: JUnit
	// Input(s): ""
	// Expected Output: "No Delivery Area entered"

	public void testvalidateDeliveryArea001() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validateDeliveryArea("");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery area not entered", e.getMessage());
		}
	}

	// Test No: 25
	// Objective: Verify 1 input delivery area is invalid
	// Test Type: JUnit
	// Input(s): "W"
	// Expected Output: "No Delivery Area entered"

	public void testvalidateDeliveryArea002() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validateDeliveryArea("W");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery area does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 26
	// Objective: Verify 2 input delivery area is invalid
	// Test Type: JUnit
	// Input(s): "WE"
	// Expected Output: "No Delivery Area entered"

	public void testvalidateDeliveryArea003() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validateDeliveryArea("WE");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery area does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No: 27
	// Objective: Verify Delivery area is valid
	// Test Type: JUnit
	// Input(s): "WEM"
	// Expected Output: "Delivery Area entered"

	public void testvalidateDeliveryArea004() throws DeliverypersonExceptionHandler {
		try {
			String result = Deliveryperson.validateDeliveryArea("WEM");
			assertEquals("Delivery area valid", result);
		} catch (DeliverypersonExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No: 28
	// Objective: Verify an input of 4 for delivery area is invalid
	// Test Type: JUnit
	// Input(s): "WEMM"
	// Expected Output: "Delivery Area entered is too long"

	public void testvalidateDeliveryArea005() throws DeliverypersonExceptionHandler {
		try {
			Deliveryperson.validateDeliveryArea("WEMM");
			fail("Exception expected");
		} catch (DeliverypersonExceptionHandler e) {
			assertEquals("Delivery area exceeds maximum length requirements", e.getMessage());
		}
	}
}