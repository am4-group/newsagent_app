import junit.framework.TestCase;

public class InvoiceTest extends TestCase {
	
//	Test no.:			1
	
//	Test objective:		To test for empty inputs as invalid
	
//	Input(s):			Customer First Name:""
//						Customer Last Name:""
//						Customer Eircode:""
//						Company Name:""
//						Company Eircode:""
//						Item Name:""
//						Total Price:""
	
//	Expected Output:	"No input detected for Customer first name, please enter a name"
	
	public void testInvoice001() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("","","","",null,"","");
			Publication testObjp = new Publication("",0.0,"","");
			Invoice testObj = new Invoice(testObjc,"","",testObjp,0.0);
			fail("Exception Expected");
		}catch(CustomerExceptionHandler e) {
			assertEquals("Customer name not entered",e.getMessage());
		}
	}
//		
//		
//	Test no.:			2
	
//	Test objective:		To test for values below minimum as invalid
	
//	Input(s):			Customer First Name:"a"
//						Customer Last Name:"b"
//						Customer Eircode:"c"
//						Company Name:"d"
//						Company Eircode:"e"
//						Item Name:"f"
//						Total Price:"0.01"
	
//	Expected Output:	"Customer first name does not meet minimum requirements,
//		please enter a valid name between 2-30 characters"

	public void testInvoice002() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("a","b","","c",null,"", "");
			Publication testObjp = new Publication("f",0.0,"","");
			Invoice testObj = new Invoice(testObjc,"d","e",testObjp,0.01);
			fail("Exception Expected");
		}catch(CustomerExceptionHandler e) {
			assertEquals("Customer name does not meet minimum length requirements",e.getMessage());
		}
	}
	
//		
//		
//	Test no.:			3
	
//	Test objective:		To test for values below minimum as invalid
	
//	Input(s):			Customer First Name:"a"
//						Customer Last Name:"b"
//						Customer Eircode:"c135c3"
//						Company Name:"d"
//						Company Eircode:"e625ds"
//						Item Name:"Paper"
//						Total Price:"1.99"
	
//	Expected Output:	"Customer first name does not meet minimum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testInvoice003() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("a","b","","c135c3",null,"", "");
			Publication testObjp = new Publication("Paper",0.0,"","");
			Invoice testObj = new Invoice(testObjc,"d","e625ds",testObjp,1.99);
			fail("Exception Expected");
		}catch(CustomerExceptionHandler e) {
			assertEquals("Customer name does not meet minimum length requirements",e.getMessage());
		}
	}
	
//		
//	Test no.:			4
	
//	Test objective:		To test for valid range of values
	
//	Input(s):			Customer First Name:"Jo"
//						Customer Last Name:"Li"
//						Customer Eircode:"s5d47e5"
//						Company Name:"CR"
//						Company Eircode:"a56s8d2"
//						Item Name:"Herald"
//						Total Price:"2.00"
	
//	Expected Output:	"Invoice Created"
	
	public void testInvoice004() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("Jo","Li","0860866666","s5d47e5",20,"WEM", "Home");
			Publication testObjp = new Publication("Herald",2.00,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","daily");
			Invoice testObj = new Invoice(testObjc,"CR","a56s8d2",testObjp,2.00);
			assertEquals("Jo",testObjc.getName());
			assertEquals("Li",testObjc.getSurname());
			assertEquals("s5d47e5",testObjc.getEircode());
			assertEquals("CR",testObj.getCompanyName());
			assertEquals("a56s8d2",testObj.getCompanyEircode());
			assertEquals("Herald",testObjp.getPublicationName());
			assertEquals(2.00,testObj.getTotalPrice());
			
		}catch(InvoiceExceptionHandler e) {
			fail("Exception Not Expected");
		}
	}
//		
//	Test no.:			5
	
//	Test objective:		To test for valid range of values
	
//	Input(s):			Customer First Name:"AAAAAAAAAAAAAAAaaaaaaaaaaaaaaa"
//						Customer Last Name:"BBBBBBBBBBBBBBBBBBbbbbbbbbbbbb"
//						Customer Eircode:"A3d 4567"
//						Company Name:"Michael and Jo Newsagent Store"
//						Company Eircode:"t65 s8d2"
//						Item Name:"Irish Farmers Journal"
//						Total Price:"2.00"
	
//	Expected Output:	"Invoice Created"
	
	public void testInvoice005() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("AAAAAAAAAAAAAAAaaaaaaaaaaaaaaa","BBBBBBBBBBBBBBBBBBbbbbbbbbbbbb","0860866666","A3d 4567",20,"WEM", "Away");
			Publication testObjp = new Publication("Irish Farmers Journal",2.00,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","daily");
			Invoice testObj = new Invoice(testObjc,
					"Michael and Jo Newsagent Store",
					"t65 s8d2",
					testObjp,
					2.00);
			assertEquals("AAAAAAAAAAAAAAAaaaaaaaaaaaaaaa",testObj.getCustomerFirstName());
			assertEquals("BBBBBBBBBBBBBBBBBBbbbbbbbbbbbb",testObj.getCustomerLastName());
			assertEquals("A3d 4567",testObjc.getEircode());
			assertEquals("Michael and Jo Newsagent Store",testObj.getCompanyName());
			assertEquals("t65 s8d2",testObj.getCompanyEircode());
			assertEquals("Irish Farmers Journal",testObjp.getPublicationName());
			assertEquals(2.00,testObj.getTotalPrice());
			
		}catch(InvoiceExceptionHandler e) {
			fail("Exception Not Expected");
		}
	}
//		
//	Test no.:			6
	
//	Test objective:		To test for values exceeding maximum as invalid
	
//	Input(s):			Customer First Name:"AAAAAAAAAAAAAAAaaaaaaaaaaaaaaaX"
//						Customer Last Name:"BBBBBBBBBBBBBBBBBBbbbbbbbbbbbbX"
//						Customer Eircode:"A3d 45672"
//						Company Name:"Michael and Jo Newsagent Stores"
//						Company Eircode:"t65 s8d21"
//						Item Name:"Irish Farmers Journals"
//						Total Price:"2.00"
	
//	Expected Output:	"Customer first name exceeds maximum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testInvoice006() throws InvoiceExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler{
		try {
			Customer testObjc = new Customer("AAAAAAAAAAAAAAAaaaaaaaaaaaaaaaX","BBBBBBBBBBBBBBBBBBbbbbbbbbbbbbX","","A3d 45672",null,"", "Home");
			Publication testObjp = new Publication("Irish Farmers Journals",3.0,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","daily");
			Invoice testObj = new Invoice(testObjc,"Michael and Jo Newsagent Stores","t65 s8d21",testObjp,2.00);
			fail("Exception Expected");
		}catch(CustomerExceptionHandler e) {
			assertEquals("Customer name exceeds maximum length requirements",e.getMessage());
		}
	}
//	FOR INPUT	Customer Last Name ///////////////////////////////////////////////////////////
	
//	Test No.:			7
//	Test Objective:		To test Customer first name with no characters as invalid
//	Input(s):			""
//	Expected Output:	"No input detected for Customer first name , please enter a name"
	public void testValidateCustomerFirstName001() {
		try {
			Invoice.validateCustomerFirstName("");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("No input detected for Customer first name, please enter a name",e.getMessage());
		}
	}
//		
//	Test No.:			8
//	Test Objective:		To test Customer first name with 1 character as invalid
//	Input(s):			"a"
//	Expected Output:	"Customer first name does not meet minimum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testValidateCustomerFirstName002() {
		try {
			Invoice.validateCustomerFirstName("a");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("Customer first name does not meet minimum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
//		
//	Test No.:			9
//	Test Objective:		To test Customer first name between 2-30 characters as valid
//	Input(s):			"Jo"
//	Expected Output:	"Customer first name accepted"
	public void testValidateCustomerFirstName003() {
		try {
			assertEquals("Customer First Name accepted",Invoice.validateCustomerFirstName("Jo"));
		} catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:			10
//	Test Objective:		To test Customer first name between 2-30 characters as valid
//	Input(s):			"abcdefghijklmnopqrstuvwxyzabcd"
//	Expected Output:	"Customer first name accepted"
	
	public void testValidateCustomerFirstName004() {
		try {
			assertEquals("Customer First Name accepted",Invoice.validateCustomerFirstName("abcdefghijklmnopqrstuvwxyzabcd"));
		} catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:			11
//	Test Objective:		To test Customer first name with 31 characters and up as invalid
//	Input(s):			"abcdefghijklmnopqrstuvwxyzabcde"
//	Expected Output:	"Customer first name exceeds maximum requirements,
//		please enter a valid name between 2-30 characters"
	public void testValidateCustomerFirstName005() {
		try {
			Invoice.validateCustomerFirstName("abcdefghijklmnopqrstuvwxyzabcde");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("Customer first name exceeds maximum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
	
//		
//	FOR INPUT	Customer Last Name ///////////////////////////////////////////////////////////
//		
//	Test No.:	12
//	Test Objective:	To test Customer last name with no characters as invalid
//	Input(s):	""
//	Expected Output:	"No input detected for Customer last name, please enter a name"
	
	public void testValidateCustomerLastName001() {
		try {
			Invoice.validateCustomerLastName("");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("No input detected for Customer last name, please enter a name",e.getMessage());
		}
	}
//		
//	Test No.:	13
//	Test Objective:	To test Customer last name with 1 character as invalid
//	Input(s):	"a"
//	Expected Output:	"Customer last name does not meet minimum requirements,
//		please enter a valid name between 2-30 characters"
	public void testValidateCustomerLastName002() {
		try {
			Invoice.validateCustomerLastName("a");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("Customer last name does not meet minimum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
//		
//	Test No.:	14
//	Test Objective:	To test Customer last name between 2-30 characters as valid
//	Input(s):	"Jo"
//	Expected Output:	"Customer last name accepted"
	public void testValidateCustomerLastName003() {
		try {
			assertEquals("Customer Last Name accepted",Invoice.validateCustomerLastName("Jo"));
		} catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	15
//	Test Objective:	To test Customer last name between 2-30 characters as valid
//	Input(s):	"abcdefghijklmnopqrstuvwxyzabcd"
//	Expected Output:	"Customer last name accepted"
	public void testValidateCustomerLastName004() {
		try {
			assertEquals("Customer Last Name accepted",Invoice.validateCustomerLastName("abcdefghijklmnopqrstuvwxyzabcd"));
		} catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	16
//	Test Objective:	To test Customer last name with 31 characters as invalid
//	Input(s):	"abcdefghijklmnopqrstuvwxyzabcde"
//	Expected Output:	"Customer last name exceeds maximum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testValidateCustomerLastName005() {
		try {
			Invoice.validateCustomerLastName("abcdefghijklmnopqrstuvwxyzabcde");
			fail("Exception Expected");
			
		} catch(InvoiceExceptionHandler e) {
			assertEquals("Customer last name exceeds maximum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
//		
//	FOR INPUT	Customer Eircode ///////////////////////////////////////////////////////////
//		
//	Test No.:	17
//	Test Objective:	To test Customer eircode with empty value as invalid
//	Input(s):	""
//	Expected Output:	"No input detected for Customer eircode, please enter an eircode"
	
	public void testValidateCustomerEircode001() {
		try {
			Invoice.validateCustomerEircode("");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("No input detected for Customer eircode, please enter an eircode",e.getMessage());
		}
	}
//		
//	Test No.:	18
//	Test Objective:	To test Customer eircode between 1-6 digits as invalid
//	Input(s):	"a"
//	Expected Output:	"Customer eircode does not meet minimum requirements,
//		please enter a valid eircode with 7-8 digits"
	public void testValidateCustomerEircode002() {
		try {
			Invoice.validateCustomerEircode("a");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Customer eircode does not meet minimum requirements, please enter a valid eircode with 7-8 digits",e.getMessage());
		}
	}
//		
//	Test No.:	19
//	Test Objective:	To test Customer eircode between 1-6 digits as invalid
//	Input(s):	"abcdef"
//	Expected Output:	"Customer eircode does not meet minimum requirements,
//		please enter a valid eircode with 7-8 digits"
	public void testValidateCustomerEircode003() {
		try {
			Invoice.validateCustomerEircode("abcdef");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Customer eircode does not meet minimum requirements, please enter a valid eircode with 7-8 digits",e.getMessage());
		}
	}
//		
//	Test No.:	20
//	Test Objective:	To test Customer eircode with 7 or 8 digits as valid
//	Input(s):	"N91W929"
//	Expected Output:	"Customer eircode accepted"
	
	public void testValidateCustomerEircode004() {
		try {
			assertEquals("Customer eircode accepted",Invoice.validateCustomerEircode("N91W929"));
		}
		catch(InvoiceExceptionHandler e) {
		fail("Exception not Expected");
		}
	}
//		
//	Test No.:	21
//	Test Objective:	To test Customer eircode with 7 or 8 digits as valid
//	Input(s):	"N91 W929"
//	Expected Output:	"Customer eircode accepted"
	
	public void testValidateCustomerEircode005() {
		try {
			assertEquals("Customer eircode accepted",Invoice.validateCustomerEircode("N91 W929"));
		}
		catch(InvoiceExceptionHandler e) {
		fail("Exception not Expected");
		}
	}
//		
//	Test No.:	22
//	Test Objective:	To test Customer eircode with 9 digits and above as invalid
//	Input(s):	"N91 W9239"
//	Expected Output:	"Customer eircode exceeds maximum requirements,
//		please enter a valid eircode with 7-8 digits"
	
	public void testValidateCustomerEircode006() {
		try {
			Invoice.validateCustomerEircode("N91 W9239");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Customer eircode exceeds maximum requirements, please enter a valid eircode with 7-8 digits",e.getMessage());
		}
	}
//		
//	FOR INPUT	Company Name ///////////////////////////////////////////////////////////
//	Test No.:	23
//	Test Objective:	To test Company name with no characters as invalid
//	Input(s):	""
//	Expected Output:	"No input detected for Company name , please enter a name"
	public void testValidateCompanyName001() {
		try {
				Invoice.validateCompanyName("");
				fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("No input detected for Company name, please enter a name",e.getMessage());
		}
	}
//		
//	Test No.:	24
//	Test Objective:	To test Company name with 1 character as invalid
//	Input(s):	"a"
//	Expected Output:	"Company name does not meet minimum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testValidateCompanyName002() {
		try {
				Invoice.validateCompanyName("a");
				fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Company name does not meet minimum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
//		
//	Test No.:	25
//	Test Objective:	To test Company name between 2-30 characters as valid
//	Input(s):	"ab"
//	Expected Output:	"Company name accepted"
	public void testValidateCompanyName003() {
		try {
				assertEquals("Company name accepted",Invoice.validateCompanyName("ab"));
				
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
	
//		
//	Test No.:	26
//	Test Objective:	To test Company name between 2-30 characters as valid
//	Input(s):	"abcdefghijklmnopqrstuvwxyzabcd"
//	Expected Output:	"Company name accepted"
	public void testValidateCompanyName004() {
		try {
				assertEquals("Company name accepted",Invoice.validateCompanyName("abcdefghijklmnopqrstuvwxyzabcd"));
				
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
	
	
//		
//	Test No.:	27
//	Test Objective:	To test Company name with 31 characters as invalid
//	Input(s):	"abcdefghijklmnopqrstuvwxyzabcde"
//	Expected Output:	"Company name exceeds maximum requirements,
//		please enter a valid name between 2-30 characters"
	
	public void testValidateCompanyName005() {
		try {
				Invoice.validateCompanyName("abcdefghijklmnopqrstuvwxyzabcde");
				fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Company name exceeds maximum requirements, please enter a valid name between 2-30 characters",e.getMessage());
		}
	}
//		
//	FOR INPUT	Company Eircode ///////////////////////////////////////////////////////////
//		
//	Test No.:	28
//	Test Objective:	To test Company eircode with empty value as invalid
//	Input(s):	""
//	Expected Output:	"No input detected for Company eircode, please enter an eircode"
	
	public void testValidateCompanyEircode001() {
		try {
			Invoice.validateCompanyEircode("");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("No input detected for Company eircode, please enter an eircode",e.getMessage());
		}
	}
//		
//	Test No.:	29
//	Test Objective:	To test Company eircode between 1-6 digits as invalid
//	Input(s):	"a"
//	Expected Output:	"Company eircode does not meet minimum requirements,
//		please enter a valid eircode between 7-8 digits"
	
	public void testValidateCompanyEircode002() {
		try {
			Invoice.validateCompanyEircode("a");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Company eircode does not meet minimum requirements, please enter a valid eircode between 7-8 digits",e.getMessage());
		}
	}
//		
//	Test No.:	30
//	Test Objective:	To test Company eircode between 1-6 digits as invalid
//	Input(s):	"a12345"
//	Expected Output:	"Company eircode does not meet minimum requirements,
//		please enter a valid eircode between 7-8 digits"
	public void testValidateCompanyEircode003() {
		try {
			Invoice.validateCompanyEircode("a12345");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Company eircode does not meet minimum requirements, please enter a valid eircode between 7-8 digits",e.getMessage());
		}
	}
//		
//	Test No.:	31
//	Test Objective:	To test Company  eircode with 7 or 8 digits as valid
//	Input(s):	"N91W929"
//	Expected Output:	"Company eircode accepted"
	public void testValidateCompanyEircode004(){
		try {
			assertEquals("Company eircode accepted",Invoice.validateCompanyEircode("N91W929"));
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	32
//	Test Objective:	To test Company eircode with 7 or 8 digits as valid
//	Input(s):	"N91 W929"
//	Expected Output:	"Company eircode accepted"
	public void testValidateCompanyEircode005(){
		try {
			assertEquals("Company eircode accepted",Invoice.validateCompanyEircode("N91 W929"));
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	33
//	Test Objective:	To test Company eircode with 9 digits and above as invalid
//	Input(s):	"N91 W9239"
//	Expected Output:	"Company exceeds maximum requirements,
//		please enter a valid eircode between 7-8 digits"
	
	public void testValidateCompanyEircode006() {
		try {
			Invoice.validateCompanyEircode("N91 W9239");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
		assertEquals("Company exceeds maximum requirements, please enter a valid eircode between 7-8 digits",e.getMessage());
		}
	}
//		
//	FOR INPUT	Item Names ///////////////////////////////////////////////////////////
//		
//	Test No.:	34
//	Test Objective:	To test item name with empty value
//	Input(s):	""
//	Expected Output:	"Invalid item name, enter an item name"
//	
	public void testValidateItemName001() {
		try {
			Invoice.validateItemName("");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Invalid item Name, please enter a valid name between 6-21 characters",e.getMessage());
		}
	}
	
//	Test No.:	35
//	Test Objective:	To test item name between 1-5 characters as invalid
//	Input(s):	"a"
//	Expected Output:	"Item name does not meet minimum requirements,
//		enter a valid item name between 6-21 characters"
	public void testValidateItemName002() {
		try {
			Invoice.validateItemName("a");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Item name does not meet minimum requirements, enter a valid item name between 6-21 characters",e.getMessage());
		}
	}
//		
//	Test No.:	36
//	Test Objective:	To test item name between 1-5 characters as invalid
//	Input(s):	"a1234"
//	Expected Output:	"Item name does not meet minimum requirements,
//		enter a valid item name between 6-21 characters"
	public void testValidateItemName003() {
		try {
			Invoice.validateItemName("a1234");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Item name does not meet minimum requirements, enter a valid item name between 6-21 characters",e.getMessage());
		}
	}
//		
//	Test No.:	37
//	Test Objective:	To test item name between 6-21 characters as valid
//	Input(s):	"Herald"
//	Expected Output:	"Item name accepted"
	
	public void testValidateItemName004() {
		try {
			assertEquals("Item name accepted", Invoice.validateItemName("Herald"));
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	38
//	Test Objective:	To test item name between 6-21 characters as valid
//	Input(s):	"Irish Farmers Journal"
//	Expected Output:	"Item name accepted"
	
	public void testValidateItemName005() {
		try {
			assertEquals("Item name accepted", Invoice.validateItemName("Irish Farmers Journal"));
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
	}
//		
//	Test No.:	39
//	Test Objective:	To test item name with 22 characters - MAXINT as invalid
//	Input(s):	"Irish Farmers Journals"
//	Expected Output:	Item name exceeds maximum requirements,
//		please enter a valid item name between 6-21 characters"
	
	public void testValidateItemName006() {
		try {
			Invoice.validateItemName("Irish Farmers Journals");
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Item name exceeds maximum requirements, please enter a valid item name between 6-21 characters",e.getMessage());
		}
	}
//		
//	FOR INPUT	Total Price ///////////////////////////////////////////////////////////
//		
//	Test No.:	40
//	Test Objective:	To test total price with amount equal to 0.0 as invalid
//	Input(s):	"0.0"
//	Expected Output:	"Total price invalid, enter a total price"
	public void testValidateTotalPrice001() {
		try {
			Invoice.validateTotalPrice(0.0);
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Total price invalid, enter a total price",e.getMessage());
		}
		
	}
//		
//	Test No.:	41
//	Test Objective:	To test total price with amount equal between �0.01 to �1.99 as invalid
//	Input(s):	"0.01"
//	Expected Output:	"Total price invalid, total price must be atleast �2.00 or higher"
	public void testValidateTotalPrice002() {
		try {
			Invoice.validateTotalPrice(0.01);
			fail("Exception Expected");
		}
		catch(InvoiceExceptionHandler e) {
			assertEquals("Total price invalid, total price must be atleast �2.00 or higher",e.getMessage());
		}
		
	}
//		
//	Test No.:	42
//	Test Objective:	To test total price with amount equal between �0.01 to �1.99 as invalid
//	Input(s):	"1.99"
//	Expected Output:	"Total price invalid, total price must be atleast �2.00 or more"
		public void testValidateTotalPrice003() {
			try {
				Invoice.validateTotalPrice(1.99);
				fail("Exception Expected");
			}
			catch(InvoiceExceptionHandler e) {
				assertEquals("Total price invalid, total price must be atleast �2.00 or higher",e.getMessage());
			}
		
	}
//		
//	Test No.:	43
//	Test Objective:	To test total price with amount equal to �2.00 or more as valid
//	Input(s):	"2.00"
//	Expected Output:	"Total price valid"
	public void testValidateTotalPrice004() {
		try {
			assertEquals("Total Price accepted", Invoice.validateTotalPrice(2.00));
		}
		catch(InvoiceExceptionHandler e) {
			fail("Exception not Expected");
		}
		
	}


	


}
