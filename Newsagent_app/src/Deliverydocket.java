
public class Deliverydocket {
	
	private int id;
	private String firstname;
	private String lastname;
	private String eircode;
	private String itemname;
	private String itemdescription;
	
	
	public Deliverydocket() throws DeliverydocketExceptionHandler{
		throw new RuntimeException("No Code Written");
	}
	
	public Deliverydocket(String firstname, String lastname, String eircode, String itemname, String itemdescription)
		throws DeliverydocketExceptionHandler{
		
		id = 0;
		
		try {
			
			validatefirstname(firstname);
			validatelastname(lastname);
			validateeircode(eircode);
			validateitemname(itemname);
			validateitemdescription(itemdescription);
			
		}catch (DeliverydocketExceptionHandler e) {
			throw e;
		}
		
		this.firstname = firstname;
		this.lastname = lastname;
		this.eircode= eircode;
		this.itemname = itemname;
		this.itemdescription= itemdescription;

	}
	
	public static String validatefirstname(String firstname) throws DeliverydocketExceptionHandler{
		if (firstname.isBlank() || firstname.isEmpty())
			throw new DeliverydocketExceptionHandler("First name has not been entered");
		else if (firstname.length() < 2)
			throw new DeliverydocketExceptionHandler("First name is not long enough");
		else if (firstname.length() > 30)
			throw new DeliverydocketExceptionHandler("First name is too long");
		else
			return "First name is valid";
	}
	
	public static String validatelastname(String lastname) throws DeliverydocketExceptionHandler{
		if (lastname.isBlank() || lastname.isEmpty())
			throw new DeliverydocketExceptionHandler("Last name has not been entered");
		else if (lastname.length() < 2)
			throw new DeliverydocketExceptionHandler("Last name is not long enough");
		else if (lastname.length() > 30)
			throw new DeliverydocketExceptionHandler("Last name is too long");
		else
			return "Last name is valid";
	}
	
	public static String validateeircode(String eircode) throws DeliverydocketExceptionHandler{
		if (eircode.isBlank() || eircode.isEmpty())
			throw new DeliverydocketExceptionHandler("Eircode has not been entered");
		else if (eircode.length() < 7)
			throw new DeliverydocketExceptionHandler("Eircode is not long enough");
		else if (eircode.length() > 8)
			throw new DeliverydocketExceptionHandler("Eircode is too long");
		else
			return "Eircode is valid";
	}
	
	public static String validateitemname(String itemname) throws DeliverydocketExceptionHandler{
		if (itemname.isBlank() || itemname.isEmpty())
			throw new DeliverydocketExceptionHandler("Item name has not been entered");
		else if (itemname.length() < 6)
			throw new DeliverydocketExceptionHandler("Item name is not long enough");
		else if (itemname.length() > 21)
			throw new DeliverydocketExceptionHandler("Item name is too long");
		else 
			return "Item name is valid";
	}
	
	public static String validateitemdescription(String itemdescription) throws DeliverydocketExceptionHandler{
		if (itemdescription.isBlank() || itemdescription.isEmpty())
			throw new DeliverydocketExceptionHandler("Item description has not been entered");
		else if (itemdescription.length() < 100)
			throw new DeliverydocketExceptionHandler("Item description is not long enough");
		else if (itemdescription.length() > 200)
			throw new DeliverydocketExceptionHandler("Item description is too long");
		else
			return "Item description is valid";
	}
	
	public int getid() {
		return id;
	}
	
	public void setid(int id) {
		this.id = id;
	}
	
	public String getfirstname() {
		return firstname;
	}
	
	public void setfirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getlastname() {
		return lastname;
	}
	
	public void setlastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String geteircode() {
		return eircode;
	}
	
	public void seteircode(String eircode) {
		this.eircode = eircode;
	}
	
	public String getitemname() {
		return itemname;
	}
	
	public void setitemname() {
		this.itemname = itemname;
	}
	
	public String getitemdescription() {
		return itemdescription;
	}
	
	public void setitemdescription() {
		this.itemdescription = itemdescription;
	}
	
}
