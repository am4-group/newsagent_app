import junit.framework.TestCase;

public class CustomerTest extends TestCase {

	// Test No.: 1
	// Objective: Verify valid input values will create a new customer
	// Input(s): name = "Ko", surname = "Ko", phoneNumber = "0838404458", eircode =
	// "N91P789", age = 18, deliveryArea = "WEM"
	// Expected Output: Customer created
	public void testvalidateCustomer001() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("Ko", "Ko", "0838404458", "N91P789", 18, "WEM", "Home");
			assertEquals(0, testObj.getId());
			assertEquals("Ko", testObj.getName());
			assertEquals("Ko", testObj.getSurname());
			assertEquals("0838404458", testObj.getPhoneNumber());
			assertEquals("N91P789", testObj.getEircode());
			assertEquals(18, testObj.getAge());
			assertEquals("WEM", testObj.getDeliveryArea());
			assertEquals("Home", testObj.getAtHomeStatus());
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 2
	// Objective: Verify empty values will return an exception
	// Input(s): name = "", surname = "", phoneNumber = "", eircode = "", age = "",
	// deliveryArea = ""
	// Expected Output: Customer details are invalid
	public void testvalidateCustomer002() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("", "", "", "", null, "", "");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name not entered", e.getMessage());
		}
	}

	// Test No.: 3
	// Objective: Verify a mixture of valid and invalid inputs will return an
	// exception
	// Input(s): name = "K", surname = "K", phoneNumber = "0", eircode = "N", age =
	// Integer.MIN_VALUE, deliveryArea = "WEM"
	// Expected Output: Customer details are invalid
	public void testvalidateCustomer003() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("K", "K", "0", "N", Integer.MIN_VALUE, "WEM", "Away");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 4
	// Objective: Verify a mixture of valid and invalid inputs will return an
	// exception
	// Input(s):name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", surname =
	// "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", phoneNumber = "083840445", eircode =
	// "N82P67", age = 17, deliveryArea = ""
	// Expected Output: Customer details are invalid
	public void testvalidateCustomer004() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"083840445", "N82P67", 17, "", "Home");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 5
	// Objective: Verify a mixture of valid and invalid inputs will return an
	// exception
	// Input(s): name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", surname =
	// "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", phoneNumber = "08384044585", eircode =
	// "N82 P678", age = Integer.MAX_VALUE, deliveryArea = "WEM"
	// Expected Output: Customer details are invalid
	public void testvalidateCustomer005() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"08384044585", "N82 P678", Integer.MAX_VALUE, "WEM", "Away");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 6
	// Objective: Verify a mixture of valid and invalid inputs will return an
	// exception
	// Input(s): name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", surname =
	// "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", phoneNumber =
	// "08384044580838404458", eircode = "N82P67894", age = 17, deliveryArea = ""
	// Expected Output:Customer details are invalid
	public void testvalidateCustomer006() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
					"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "08384044580838404458", "N82P67894", 17, "", "Home");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 7
	// Objective: Verify a mixture of valid and invalid inputs will return an
	// exception
	// Input(s): name = "Ko", surname = "Ko", phoneNumber = "0838404458", eircode =
	// "N82P678944", age = 17, deliveryArea = "WEM"
	// Expected Output:Customer details are invalid
	public void testvalidateCustomer007() throws CustomerExceptionHandler {
		try {
			Customer testObj = new Customer("Ko", "Ko", "0838404458", "N82P678944", 17, "WEM", "Away");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode exceeds maximum length requirements", e.getMessage());
		}
	}

	// =============================================================================================
	// Test No.: 8
	// Objective: Verify empty name will return "Customer name not entered"
	// Input(s): name = ""
	// Expected Output: Customer name not entered
	public void testvalidateName001() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateName("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name not entered", e.getMessage());
		}
	}

	// Test No.: 9
	// Objective: Verify name of length 1 will return "Customer name does not meet
	// minimum length requirements"
	// Input(s): name = "K"
	// Expected Output: Customer name does not meet minimum length requirements
	public void testvalidateName002() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateName("K");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 10
	// Objective: Verify name of length 2 will return "Customer name valid"
	// Input(s): name = "Ko"
	// Expected Output: Customer name valid
	public void testvalidateName003() throws CustomerExceptionHandler {
		try {
			// Call method under test
			String result = Customer.validateName("Ko");
			assertEquals("Customer name valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 11
	// Objective: Verify name of length 30 will return "Customer name valid"
	// Input(s): name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer name valid
	public void testvalidateName004() throws CustomerExceptionHandler {
		try {
			// Call method under test
			String result = Customer.validateName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			assertEquals("Customer name valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 12
	// Objective: Verify name of length 31 will return "Customer name exceeds
	// maximum length requirements"
	// Input(s): name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer name exceeds maximum length requirements
	public void testvalidateName005() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 13
	// Objective: Verify name of length 40 will return "Customer name exceeds
	// maximum length requirements"
	// Input(s): name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer name exceeds maximum length requirements
	public void testvalidateName006() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer name exceeds maximum length requirements", e.getMessage());
		}
	}

	// =============================================================================================
	// Test No.: 14
	// Objective: Verify empty name will return "Customer surname not entered"
	// Input(s): surname = ""
	// Expected Output: Customer surname not entered
	public void testvalidateSurname001() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateSurname("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer surname not entered", e.getMessage());
		}
	}

	// Test No.: 15
	// Objective: Verify name of length 1 will return "Customer surname does not
	// meet minimum length requirements"
	// Input(s): surname = "K"
	// Expected Output: Customer surname does not meet minimum length requirements
	public void testvalidateSurname002() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateSurname("K");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer surname does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 16
	// Objective: Verify name of length 2 will return "Customer surname valid"
	// Input(s): surname = "Ko"
	// Expected Output: Customer surname valid
	public void testvalidateSurname003() throws CustomerExceptionHandler {
		try {
			// Call method under test
			String result = Customer.validateSurname("Ko");
			assertEquals("Customer surname valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 17
	// Objective: Verify name of length 30 will return "Customer surname valid"
	// Input(s): surname = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer surname valid
	public void testvalidateSurname004() throws CustomerExceptionHandler {
		try {
			// Call method under test
			String result = Customer.validateSurname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			assertEquals("Customer surname valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 18
	// Objective: Verify name of length 31 will return "Customer surname exceeds
	// maximum length requirements"
	// Input(s): surname = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer surname exceeds maximum length requirements
	public void testvalidateSurname005() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateSurname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer surname exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 19
	// Objective: Verify name of length 40 will return "Customer surname exceeds
	// maximum length requirements"
	// Input(s): surname = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	// Expected Output: Customer surname exceeds maximum length requirements
	public void testvalidateSurname006() throws CustomerExceptionHandler {
		try {
			// Call method under test
			Customer.validateSurname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer surname exceeds maximum length requirements", e.getMessage());
		}
	}

	// =============================================================================================
	// Test No.: 20
	// Objective: Verify empty phone number will return "Customer phone number not
	// entered"
	// Input(s): phoneNumber = ""
	// Expected Output: Customer phone number not entered
	public void testvalidatePhoneNumber001() throws CustomerExceptionHandler {
		try {
			Customer.validatePhoneNumber("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number not entered", e.getMessage());
		}
	}

	// Test No.: 21
	// Objective: Verify phone number of length 1 will return "Customer phone number
	// does not meet minimum length requirements"
	// Input(s): phoneNumber = "0"
	// Expected Output: Customer phone number does not meet minimum length
	// requirements
	public void testvalidatePhoneNumber002() throws CustomerExceptionHandler {
		try {
			Customer.validatePhoneNumber("0");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 22
	// Objective: Verify phone number of length 9 will return "Customer phone number
	// does not meet minimum length requirements"
	// Input(s): phoneNumber = "083840445"
	// Expected Output: Customer phone number does not meet minimum length
	// requirements
	public void testvalidatePhoneNumber003() throws CustomerExceptionHandler {
		try {
			Customer.validatePhoneNumber("083840445");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 23
	// Objective: Verify phone number of length 10 will return "Customer phone
	// number valid"
	// Input(s): phoneNumber = "0838404458"
	// Expected Output: Customer phone number valid
	public void testvalidatePhoneNumber004() throws CustomerExceptionHandler {
		try {
			String result = Customer.validatePhoneNumber("0838404458");
			assertEquals("Customer phone number valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 24
	// Objective: Verify phone number of length 11 will return "Customer phone
	// number exceeds maximum length requirements"
	// Input(s): phoneNumber = "08384044589"
	// Expected Output: Customer phone number exceeds maximum length requirements
	public void testvalidatePhoneNumber005() throws CustomerExceptionHandler {
		try {
			Customer.validatePhoneNumber("08384044589");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 25
	// Objective: Verify phone number of length 20 will return "Customer phone
	// number exceeds maximum length requirements"
	// Input(s): phoneNumber = "08384044589838404458"
	// Expected Output: Customer phone number exceeds maximum length requirements
	public void testvalidatePhoneNumber006() throws CustomerExceptionHandler {
		try {
			Customer.validatePhoneNumber("08384044589838404458");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer phone number exceeds maximum length requirements", e.getMessage());
		}
	}

	// =============================================================================================
	// Test No.: 26
	// Objective: Verify empty eircode will return "Customer eircode not entered"
	// Input(s): eircode = ""
	// Expected Output: Customer eircode not entered
	public void testvalidateEircode001() throws CustomerExceptionHandler {
		try {
			Customer.validateEircode("");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode not entered", e.getMessage());
		}
	}

	// Test No.: 27
	// Objective: Verift eircode of length 1 will return "Customer eircode does not
	// meet minimum length requirements"
	// Input(s): eircode = "N"
	// Expected Output: Customer eircode does not meet minimum length requirements
	public void testvalidateEircode002() throws CustomerExceptionHandler {
		try {
			Customer.validateEircode("N");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 28
	// Objective: Verift eircode of length 6 will return "Customer eircode does not
	// meet minimum length requirements"
	// Input(s): eircode = "N91P78"
	// Expected Output: Customer eircode does not meet minimum length requirements
	public void testvalidateEircode003() throws CustomerExceptionHandler {
		try {
			Customer.validateEircode("N91P78");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 29
	// Objective: Verify eircode of length 7 will return "Customer eircode valid"
	// Input(s): eircode = "N91P789"
	// Expected Output: Customer eircode valid
	public void testvalidateEircode004() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateEircode("N91P789");
			assertEquals("Customer eircode valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 30
	// Objective: Verify eircode of length 8 will return "Customer eircode valid"
	// Input(s): eircode = "N91 P789"
	// Expected Output: Customer eircode valid
	public void testvalidateEircode005() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateEircode("N91 P789");
			assertEquals("Customer eircode valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 31
	// Objective: Verify eircode of length 9 will return "Customer eircode exceeds
	// maximum length requirements"
	// Input(s): eircode = "N91 P6789"
	// Expected Output: Customer eircode exceeds maximum length requirements
	public void testvalidateEircode006() throws CustomerExceptionHandler {
		try {
			Customer.validateEircode("N91 P6789");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 32
	// Objective: Verify eircode of length 9 will return "Customer eircode exceeds
	// maximum length requirements"
	// Input(s): eircode = "N12345678P12345"
	// Expected Output: Customer eircode exceeds maximum length requirements
	public void testvalidateEircode007() throws CustomerExceptionHandler {
		try {
			Customer.validateEircode("N12345678P12345");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer eircode exceeds maximum length requirements", e.getMessage());
		}
	}

	// =============================================================================================
	// Test No.: 33
	// Objective: Verify empty age will return "Customer age not entered"
	// Input(s): age = ""
	// Expected Output: Customer age not entered
	public void testvalidateAge001() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateAge(null);
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer age not entered", e.getMessage());
		}
	}

	// Test No.: 34
	// Objective: Verify age of length Integer.MIN_VALUE will return "Customer age
	// does not meet minimum length requirements"
	// Input(s): age = Integer.MIN_VALUE
	// Expected Output: Customer age does not meet minimum length requirements
	public void testvalidateAge002() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateAge(Integer.MIN_VALUE);
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer age does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 35
	// Objective: Verify age of length 17 will return "Customer age does not meet
	// minimum length requirements"
	// Input(s): age = 17
	// Expected Output: Customer age does not meet minimum length requirements
	public void testvalidateAge003() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateAge(17);
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer age does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 36
	// Objective: Verify age of length 18 will return "Customer age valid"
	// Input(s): age = 18
	// Expected Output: Customer age valid
	public void testvalidateAge004() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateAge(18);
			assertEquals("Customer age valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 37
	// Objective: Verify age of length Integer.MAX_VALUE will return "Customer age
	// valid"
	// Input(s): age = Integer.MAX_VALUE
	// Expected Output: Customer age valid
	public void testvalidateAge005() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateAge(Integer.MAX_VALUE);
			assertEquals("Customer age valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// =============================================================================================
	// Test No.:38
	// Objective: Verify empty delivery area will return "Customer delivery area not
	// entered"
	// Input(s): deliveryArea = ""
	// Expected Output: Customer delivery area not entered
	public void testvalidateDeliveryArea001() throws CustomerExceptionHandler {
		try {
			Customer.validateDeliveryArea("");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer delivery area not entered", e.getMessage());
		}
	}

	// Test No.: 39
	// Objective: Verify that delivery of length 2 will return "Customer delivery area does not meet minimum length requirements"
	// Input(s): deliveryArea = "WEM"
	// Expected Output: Customer delivery area valid
	public void testvalidateDeliveryArea002() throws CustomerExceptionHandler {
		try {
			Customer.validateDeliveryArea("WE");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer delivery area does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 40
	// Objective: Verify that that delivery area of length 3 will return "Customer delivery area valid"
	// delivery area valid"
	// Input(s): deliveryArea = "WEM"
	// Expected Output: Customer delivery area valid
	public void testvalidateDeliveryArea003() throws CustomerExceptionHandler {
		try {
			String result = Customer.validateDeliveryArea("WEM");
			assertEquals("Customer delivery area valid", result);
		} catch (CustomerExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 41
	// Objective: Verify that delivery area of length 4 will return "Customer delivery area exceeds maximum length requirements"
	// Input(s): deliveryArea = "WEM"
	// Expected Output: Customer delivery area valid
	public void testvalidateDeliveryArea004() throws CustomerExceptionHandler {
		try {
			Customer.validateDeliveryArea("WEMM");
			fail("Exception expected");
		} catch (CustomerExceptionHandler e) {
			assertEquals("Customer delivery area exceeds maximum length requirements", e.getMessage());
		}
	}
}
