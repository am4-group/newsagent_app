
public class DeliveryareaExceptionHandler extends Exception {
	
	String message;
	
	public DeliveryareaExceptionHandler(String errMessage) {
		
		message = errMessage;
	}
	
	public String getMessage() {
		
		return message;
	}

}