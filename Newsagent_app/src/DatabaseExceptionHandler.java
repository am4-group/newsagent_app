
public class DatabaseExceptionHandler extends Exception{

	String message;
	
	public DatabaseExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
