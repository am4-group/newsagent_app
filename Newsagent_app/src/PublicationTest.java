import junit.framework.TestCase;


public class PublicationTest extends TestCase {

	// Test No.: 		1
	
	// Objective: 		To test for the first range of invalid input values
	
	// Input(s): 		Name = "J"
	// 					Price = 0.01
	// 					Description = "a"
	// 					Frequency = ""
	
	// Expected Output: "Publication Name does not meet minimum length characters (must be greater than 5)"
	
	public void testPublication001() throws PublicationExceptionHandler{
		try {
			Publication testObj = new Publication("J",0.01,"a","");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e){
			assertEquals("Publication Name does not meet minimum length characters (must be greater than 5)",e.getMessage());
		}
	}

	// Test No.:		2
	
	// Objective: 		To test for the first range of invalid input values
	
	// Input(s): 		Name = "Today"
	// 					Price = 1.99
	// 					Description = *description with 99 characters*
	// 					Frequency = ""
	
	// Expected Output: "Publication Name does not meet minimum length characters (must be greater than 5)"
	
	public void testPublication002(){
		try {
			Publication testObj = new Publication("Today",1.99,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu","");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e){
			assertEquals("Publication Name does not meet minimum length characters (must be greater than 5)",e.getMessage());
		}
	}


	// Test No. 		3
	
	// Objective:	 	To test for the range of valid input values
	
	// Input(s): 		Name = "Herald"
	// 					Price = 2.00
	// 					Description = *description with 100 characters*
	// 					Frequency = "daily"
	
	// Expected Output: Publication accepted
	
	public void testPublication003(){
		try {
			Publication testObj = new Publication("Herald",2.00,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","daily");
			
			assertEquals("Herald",testObj.getPublicationName());
			assertEquals(2.00,testObj.getPublicationPrice());
			assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv",testObj.getPublicationDescription());
			assertEquals("daily",testObj.getPublicationFrequency());
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 		4
	
	// Objective: 		To test for the range of valid input values
	
	// Input(s):	 	Name = "Irish Farmers Journal"
	// 					Price = 4.00
	// 					Description = *description with 200 characters*
	// 					Frequency = "weekly"
	
	// Expected Output: Publication accepted
	
	public void testPublication004(){
		try {
			Publication testObj = new Publication("Irish Farmers Journal",4.00,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","weekly");
			
			assertEquals("Irish Farmers Journal",testObj.getPublicationName());
			assertEquals(4.00,testObj.getPublicationPrice());
			assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv",testObj.getPublicationDescription());
			assertEquals("weekly",testObj.getPublicationFrequency());
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 		5
	
	// Objective: 		To test for the range of valid input values
	
	// Input(s): 		Name = "Irish Farmers Journal"
	// 					Price = 4.00
	// 					Description = *description with 200 characters*
	// 					Frequency = "monthly"
	
	// Expected Output: Publication accepted
	
	public void testPublication005(){
		try {
			Publication testObj = new Publication("Irish Farmers Journal",4.00,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv","monthly");
			
			assertEquals("Irish Farmers Journal",testObj.getPublicationName());
			assertEquals(4.00,testObj.getPublicationPrice());
			assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv",testObj.getPublicationDescription());
			assertEquals("monthly",testObj.getPublicationFrequency());
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 		6
	
	// Objective: 		To test for the second range of invalid input values
	
	// Input(s): 		Name = "Irish Farmers Journals"
	// 					Price = 4.01
	// 					Description = *description with 201 characters*
	// 					Frequency = "hourly"
	
	// Expected Output: "Publication Name exceeds maximum length requirements (must be less than 22)"

	
	public void testPublication006(){
		try {
			Publication testObj = new Publication("Irish Farmers Journals",4.01,"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw","hourly");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e){	
			assertEquals("Publication Name exceeds maximum length requirements (must be less than 22)",e.getMessage());
		}
	}
	

	// Test No.: 		7
	
	// Objective: 		To test for empty input values
	
	// Input(s): 		Name = ""
	// 					Price = "0"
	// 					Description = ""
	// 					Frequency = "anytime"
	
	//Expected Output:	Publication Name invalid, no input detected

	
	public void testPublication007(){
		try {
			Publication testObj = new Publication("",0,"","");
			fail("Exception expected");
		}
			catch(PublicationExceptionHandler e){
				assertEquals("Publication Name invalid, enter a name",e.getMessage());
		}
	}
	
	//Publication Name
	
	//Test No.:			8
	//Test Objective:	To test Publication Name with invalid range 1-5 characters	
	//Input(s):			"a"
	//Expected Output:	"Publication Name does not meet minimum length characters (must be greater than 5)"

	public void testValidatePublicationName001() {
		try {
			Publication.validatePublicationName("a");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Name does not meet minimum length characters (must be greater than 5)", e.getMessage());
		}
	}
	
	//Test No.:			9
	//Test Objective:	To test Publication Name with invalid range 1-5 characters	
	//Input(s):			"Newss"
	//Expected Output:	"Publication Name does not meet minimum length characters (must be greater than 5)"
	
	public void testValidatePublicationName002() {
		try {
			Publication.validatePublicationName("Newss");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Name does not meet minimum length characters (must be greater than 5)", e.getMessage());
		}
	}
	
	//Test No.:			10
	//Test Objective:	To test Publication Name with valid range 6-21 characters	
	//Input(s):			"Herald"
	//Expected Output:	"Publication Name accepted"
	
	public void testValidatePublicationName003() {
		try {
			assertEquals("Publication Name accepted", Publication.validatePublicationName("Herald"));
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	//Test No.:			11
	//Test Objective:	To test Publication Name with valid range 6-21 characters		
	//Input(s):			"Irish Farmers Journal"
	//Expected Output:	"Publication Name accepted"
	
	public void testValidatePublicationName004() {
		try {
			assertEquals("Publication Name accepted", Publication.validatePublicationName("Irish Farmers Journal"));
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}
	
	//Test No.:			12
	//Test Objective:	To test Publication Name with invalid range 22 - MAXINT characters	
	//Input(s):			"Irish Farmers Journals"
	//Expected Output:	"Publication Name exceeds maximum length requirements (must be less than 22)"
	
	public void testValidatePublicationName005() {
		try {
		
			Publication.validatePublicationName("Irish Farmers Journals");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Name exceeds maximum length requirements (must be less than 22)", e.getMessage());
		}
	}

	//Test No.:			13
	//Test Objective:	To test Publication Name with empty string/ no input
	//Input(s):			""
	//Expected Output:	"Publication Name invalid, enter a name"
	
	public void testValidatePublicationName006() {
		try {
			
			Publication.validatePublicationName("");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Name invalid, enter a name", e.getMessage());
		}
	}
	
	//Publication Price
	
	//Test No.:			14
	//Test Objective:	To test Publication Price between 0.01 to 1.99
	//Input(s): 		"0.01"
	//Expected Output:	"Publication Price does not meet minimum value(must be greater than �1.99)"
	
	public void testValidatePublicationPrice001() {
		try {
			Publication.validatePublicationPrice(0.01);
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Price does not meet minimum value(must be greater than �1.99)", e.getMessage());
		}
	}
	

	//Test No.:			15
	//Test Objective:	To test Publication Price between 0.01 to 1.99
	//Input(s): 		"1.99"
	//Expected Output:	"Publication Price does not meet minimum value(must be greater than �1.99)"
	
	public void testValidatePublicationPrice002() {
		try {
			
			Publication.validatePublicationPrice(1.99);
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Price does not meet minimum value(must be greater than �1.99)", e.getMessage());
		}
	}
	

	//Test No.:			16
	//Test Objective:	To test Publication Price between 2.00 to 4.00
	//Input(s): 		"2.00"
	//Expected Output:	"Publication Price accepted"
	
	public void testValidatePublicationPrice003() {
		try {
			assertEquals("Publication Price accepted", Publication.validatePublicationPrice(2.00));
		}
		catch(PublicationExceptionHandler e) {
			
			fail("Exception not expected");
		}
	}
	

	//Test No.:			17
	//Test Objective:	To test Publication Price between 2.00 to 4.00
	//Input(s): 		"4.00"
	//Expected Output:	"Publication Price accepted"
	
	public void testValidatePublicationPrice004() {
try {
	
			assertEquals("Publication Price accepted", Publication.validatePublicationPrice(4.00));
		}
		catch(PublicationExceptionHandler e) {
			
			fail("Exception not expected");
		}
	}
	

	//Test No.:			18
	//Test Objective:	To test Publication Price between 4.01 and up
	//Input(s): 		"4.01"
	//Expected Output:	"Publication Price exceeds maximum value(must be less than �4.01)"
	
	public void testValidatePublicationPrice005() {
		try {
			
			Publication.validatePublicationPrice(4.01);
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Price exceeds maximum value(must be less than �4.01)", e.getMessage());
		}
	}
	

	//Test No.:			19
	//Test Objective:	To test Publication Price with no input
	//Input(s): 		""
	//Expected Output:	"Publication Price invalid, enter a price
	
	public void testValidatePublicationPrice006() {
		try {

			Publication.validatePublicationPrice(0.0);
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Price invalid, enter a price", e.getMessage());
		}
	}
	
	//Publication Description
	
	//Test No.:			20
	//Test Objective:	To test Publication Description with character length 1-99
	//Input(s): 		"a"
	//Expected Output:	"Publication Description does not meet minimum length characters(must be greater than 99)"
	
	public void testValidatePublicationDescription001() {
		try {
			Publication.validatePublicationDescription("a");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Description does not meet minimum length characters(must be greater than 99)", e.getMessage());
		}
	}
	
	//Test No.:			21
	//Test Objective:	To test Publication Description with character length 1-99
	//Input(s): 		"<description with 99 characters>"
	//Expected Output:	"Publication Description does not meet minimum length characters(must be greater than 99)"
	
	public void testValidatePublicationDescription002() {
		try {
			
			Publication.validatePublicationDescription("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Description does not meet minimum length characters(must be greater than 99)", e.getMessage());
		}
	}
	
	//Test No.:			22
	//Test Objective:	To test Publication Description with character length 100-200
	//Input(s): 		"<description with 100 characters>"
	//Expected Output:	"Publication Description accepted"
	
	public void testValidatePublicationDescription003() {
		try {
		
			assertEquals("Publication Description accepted",Publication.validatePublicationDescription("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"));
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}
	
	//Test No.:			23
	//Test Objective:	To test Publication Description with character length 100-200
	//Input(s): 		"<description with 200 characters>"
	//Expected Output:	"Publication Description accepted"
	
	public void testValidatePublicationDescription004() {
		try {
			
			assertEquals("Publication Description accepted", Publication.validatePublicationDescription("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"));
		}
		catch(PublicationExceptionHandler e) {
			fail("Exception not expected");
		}
	}
	
	//Test No.:			24
	//Test Objective:	To test Publication Description with character length 201 and up
	//Input(s): 		"<description with 201 characters>"
	//Expected Output:	"Publication Description exceeds maximum length characters (must be less than 201)"
	
	public void testValidatePublicationDescription005() {
		try {
			
			Publication.validatePublicationDescription("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
					+ "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw");
			fail("Exception expected");
			
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Description exceeds maximum length characters (must be less than 201)",e.getMessage());
		}
	}
	
	//Test No.:			25
	//Test Objective:	To test Publication Description with no input
	//Input(s): 		""
	//Expected Output:	"Publication Description invalid, enter a description
	
	public void testValidatePublicationDescription006() {
		try {
			
			Publication.validatePublicationDescription("");
			fail("Exception expected");
			
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Description invalid, enter a description",e.getMessage());
		}
	}
	//Publication Frequency
	
	//Test No.:			26
	//Test Objective: 	To test Publication Frequency with no input as invalid
	//Input(s):			""
	//Expected Outputs:	"No input detected. Please enter a frequency"
	
	public void testValidatePublicationFrequency001() {
		try {
			Publication.validatePublicationFrequency("");
			fail("Exception expected");
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("No input detected. Please enter a frequency",e.getMessage());
		}
	}
	
	//Test No.:			27
	//Test Objective:	To test Publication Frequency "daily" as valid
	//Input(s):			"daily"
	//Expected Outputs:	"Publication Frequency has been set to daily"
	
	public void testValidatePublicationFrequency002() {
		try {
			
			assertEquals("Publication Frequency has been set to daily",Publication.validatePublicationFrequency("daily"));
			
		}
		catch(PublicationExceptionHandler e) {
			
			fail("Exception not expected");
		}
	}

	//Test No.:			28
	//Test Objective:	To test Publication Frequency "weekly" as valid
	//Input(s):			"weekly"
	//Expected Outputs:	"Publication Frequency has been set to weekly"
	
	public void testValidatePublicationFrequency003() {
		try {
		
			assertEquals("Publication Frequency has been set to weekly",Publication.validatePublicationFrequency("weekly"));
			
		}
		catch(PublicationExceptionHandler e) {
			
			fail("Exception not expected");
		}
	}
	
	//Test No.:			29
	//Test Objective:	To test Publication Frequency "monthly" as valid
	//Input(s):			"monthly"
	//Expected Outputs:	"Publication Frequency has been set to monthly"
	
	public void testValidatePublicationFrequency004() {
		try {
			
			assertEquals("Publication Frequency has been set to monthly",Publication.validatePublicationFrequency("monthly"));
		}
		catch(PublicationExceptionHandler e) {
			
			fail("Exception not expected");
		}
	}
	
	//Test No.:			30
	//Test Objective:	To test Publication Frequency with invalid option
	//Input(s):			"anytime"
	//Expected Outputs:	"Publication Frequency is invalid, enter a valid"
	
	public void testValidatePublicationFrequency005() {
		try {
			Publication.validatePublicationFrequency("anytime");
			fail("Exception expected");
			
		}
		catch(PublicationExceptionHandler e) {
			assertEquals("Publication Frequency is invalid, enter a valid",e.getMessage());
		}
	}

}