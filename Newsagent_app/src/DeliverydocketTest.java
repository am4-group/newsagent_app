import junit.framework.TestCase;

public class DeliverydocketTest extends TestCase{

//	Test No. 1
//	Test objective: To test for empty values being inputed
//	Input(s): First name: ""
//			  Last name:""
//			  Eircode:""
//			  Item name:""
//	  		  Item Description: *no description entered*	
//	Expected Output: First name is Invalid no value inputed
//					 Last name is Invalid no value inputed
//					 Eircode is Invalid  no value inputed
//					 Item name is Invalid no value inputed
//					 Item description is Invalid  no value inputed
	
	public void testvalidateCreateDeliverydocket001() throws DeliverydocketExceptionHandler{
		
		try {
			Deliverydocket testObj = new Deliverydocket("", "", "", "", "");
			
			fail("Exception expected");
		}catch (DeliverydocketExceptionHandler e) {
			assertEquals("First name has not been entered", e.getMessage());
		}
	}
	
//	Test No. 2
//	Test objective:	To test for the first range of valid input values
//	Input(s):First name: "Kyle"
//			 Last name:"Leahy"
//			 Eircode:"N49K7V6"
//			 Item name:"The Daily Sun"
//			 Item Description: *description of a number of characters*
//	Expected Output: True
	
	public void testvalidateCreateDeliverydocket002() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("Kyle", "Leahy", "N49K7V6", "The Daily Sun", "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
			assertEquals(0, testObj.getid());
			assertEquals("Kyle", testObj.getfirstname());
			assertEquals("Leahy", testObj.getlastname());
			assertEquals("N49K7V6", testObj.geteircode());
			assertEquals("The Daily Sun", testObj.getitemname());
			assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz", testObj.getitemdescription());
			
		}catch (DeliverydocketExceptionHandler e) {
			
			fail("Exception Not Expected");
			
		}
		
	}
	
	
//	Test No. 3
//	Test objective:	To test for the second range of valid input values
//	Input(s):First name: "John"
//			 Last name:"Jefferson"
//			 Eircode:"N53F8L5"
//			 Item name:"The NewYork Times"
//			 Item Description: *description of a number of characters*
//	Expected Output: True
	
	public void testvalidateCreateDeliverydocket003() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("John", "Jefferson", "N53F8L5", "The NewYork Times", "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
			
		}catch (DeliverydocketExceptionHandler e) {
			fail("Exception Not Expected");
		}
	}
	
	
//	Test No. 4
//	Test objective:To test for the first range of invalid input values
//	Input(s): First name: "AdamavajohnAdamavajohnAdamavajohnAdamavajohnAdamavajohn"
//			  Last name:"t"
//			  Eircode:"N41"
//			  Item name:"ashashashashashashashashash"
//			  Item Description: *description of a number of characters*
//	Expected Output: First name is Invalid character length too long
//					 Last name is Invalid character length too short
//					 Eircode is Invalid character length too short
//					 Item name is Invalid character length too long
//					 Item description is Invalid character length too long
	
	public void testvalidateCreateDeliverydocket004() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("AdamavajohnAdamavajohnAdamavajohnAdamavajohnAdamavajohn", "t", "N41", "ashashashashashashashashash", "abcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvw");
			
			fail("Exception expected");
			
		}catch (DeliverydocketExceptionHandler e) {
			assertEquals("First name is too long", e.getMessage());
		}
	}
	
//	Test No. 5
//	Test objective:To test for the second range of invalid input values
//	Input(s): First name: "A"
//			  Last name:"testtesttesttesttesttesttesttesttesttest"
//			  Eircode:"N41"
//			  Item name:"ash"
//			  Item Description: *description of a number of characters*
//	Expected Output: First name is Invalid character length too short
//					 Last name is Invalid character length too long
//					 Eircode is Invalid character length too long
//					 Item name is Invalid character length too short
//					 Item description is Invalid character length too short
	
	public void testvalidateCreateDeliverydocket005() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("A", "testtesttesttesttesttesttesttes", "N4175K4219V69", "as", "abr");
			
			fail("Exception expected");
			
		}catch (DeliverydocketExceptionHandler e) {
			assertEquals("First name is not long enough", e.getMessage());
		}
	}

	
//	Test No. 6
//	Test objective: To test for the first range of both valid and invalid input values
//	Input(s): First name: "Kyle"
//			  Last name:"testtesttesttesttesttesttesttest"
//			  Eircode:"N49K7V6"
//			  Item name:"News"
//	  		  Item Description: *description of a number of characters*	
//	Expected Output: Item name is Invalid character length too short
//					 Last name is Invalid character length too long
//					 Item description is Invalid character length too short

	public void testvalidateCreateDeliverydocket006() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("Kyle", "testtesttesttesttesttesttesttest", "N49K7V6", "News", "abcdefyhijkl");
			
			fail("Exception expected");
			
		}catch (DeliverydocketExceptionHandler e) {
			assertEquals("Last name is too long", e.getMessage());
		}
	}
	
//	Test No. 7
//	Test objective: To test for the second range of both valid and invalid input values
//	Input(s): First name: "J"
//			  Last name:"testtest"
//			  Eircode:"N494K748V658"
//			  Item name:"The NewYork Times"
//	  		  Item Description: *description of a number of characters*	
//	Expected Output: First name is Invalid character length too short
//					 Eircode is Invalid character length too long
//					 Item description is Invalid character length too short

	public void testvalidateCreateDeliverydocket007() throws DeliverydocketExceptionHandler {
		
		try {
			Deliverydocket testObj = new Deliverydocket("J", "testtesttesttesttesttesttesttest", "N494K748V658", "The NewYork Times", "abcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijklabcdefyhijkl");
			
			fail("Exception expected");
			
		}catch (DeliverydocketExceptionHandler e) {
			assertEquals("First name is not long enough", e.getMessage());
		}
	}
	
//	Test No. 8
//	Test objective: To verify that First name of length 0 will return "first name empty"
//	Input(s): First name: " "
//	Expected Output: First name is invalid no value entered
	
	public void testvalidatefirstname001() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatefirstname("");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("First name has not been entered", e.getMessage());
		}
	}
	
//	Test No. 9
//	Test objective: To verify that First name of length 1 will return "first name is too short"
//	Input(s): First name: "J"
//	Expected Output: First name is invalid character length is too short
	
	public void testvalidatefirstname002() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatefirstname("J");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("First name is not long enough", e.getMessage());
		}
	}
	
//	Test No. 	10
//	Test Objective:	To verify that First name of length 2 will return "first name accepted"
//	Input(s): First name: "Ji"
//	Expected Output: First name accepted

	
	public void testvalidatefirstname003() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validatefirstname("Ji");
			
			assertEquals("First name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No. 11
//	Test Objective:	To Verify that name of length 30 will return "first name accepted"
//	Input(s): First name: "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
//	Expected Output: First name accepted

	
	public void testvalidatefirstname004() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validatefirstname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			
			assertEquals("First name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
		
			fail("Exception expected");
		}
	}
	
//	Test No. 12
//	Test Objective: To Verify that name of length 31 will return "first name is too long"
//	Input(s): First name: "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
//	Expected Output: First name is invalid character length is too long

	
	public void testvalidatefirstname005() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatefirstname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Ecxeption expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("First name is too long", e.getMessage());
		}
	}
	
//	Test No. 13
//	Test Objective: To Verify that name of length 40 will return "first name is too long"
//	Input(s): First name:"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
//	Expected Output: First name is invalid character length is too long
	
	public void testvalidatefirstname006() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatefirstname("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("First name is too long", e.getMessage());
		}
	}
	
//	Test No.	14
//	Test Objective 	To Verify that last name of length 0 will return "last name empty"
//	Input(s)	Last name:" "
//	Expected Output	Last name is invalid, no value entered
	
	public void testvalidatelastname001() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatelastname("");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Last name has not been entered", e.getMessage());
		}
	}
	
//	Test No.	15
//	Test Objective	To Verify that last name of length 1 will return "last name is too short"
//	Input(s)	Last name: "F"
//	Expected Output	Last name is invalid character length is too short
	
	public void testvalidatelastname002() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatelastname("F");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Last name is not long enough", e.getMessage());
		}
	}
	
//	Test No.	16
//	Test Objective	To Verify that last name of length 2 will return "Last name accepted"
//	Input(s)	Last name:"Fi"
//	Expected Output	Last name accepted
	
	public void testvalidatelastname003() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validatelastname("Fi");
			assertEquals("Last name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No.	17
//	Test Objective	To Verify that last name of length 30 will return "Last name accepted"
//	Input(s)	Last name:"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
//	Expected Output	Last name accepted
	
	public void testvalidatelastname004() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validatelastname("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
			assertEquals("Last name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			
			fail("Exception expected");
		}
	}
	
//	Test No.	18
//	Test Objective	To Verify that last name of length 31 will return "Last name is too long"
//	Input(s)	Last name:"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
//	Expected Output	Last name is invalid character length is too long 
	
	public void testvalidatelastname005() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatelastname("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Last name is too long", e.getMessage());
		}
	}
	
//	Test No. 	19
//	Test Objective 	To Verify that last name of length 40 will return "Last name is too long"
//	Input(s)	Last name:"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
//	Expected Output 	Last name is invalid character length is too long

	public void testvalidatelastname006() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validatelastname("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
			fail("Ecxeption expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Last name is too long", e.getMessage());
		}
	}
	
//	Test No.	20
//	Test Objective 	To Verify that eircode of length 0 will return "Eircode empty"
//	Input(s)	Eircode: " "
//	Expected Output	Eircode is invalid, no value entered
	
	public void testvalidateeircode001() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateeircode("");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Eircode has not been entered", e.getMessage());
		}
	}
	
//	Test No. 	21
//	Test Objective	To Verify that eircode of length 1 will return "Eircode is too short"
//	Input(s)	Eircode:"N"
//	Expected Output	Eircode is invalid character length is too short
	
	public void testvalidateeircode002() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateeircode("N");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Eircode is not long enough", e.getMessage());
		}
	}
	
//	Test No.	22
//	Test Objective	To Verify that eircode of length 6 will return "Eircode is too short"
//	Input(s)	Eircode:"N45K8G"
//	Expected Output	Eircode is invalid character length is too short
	
	public void testvalidateeircode003() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateeircode("N45K8G");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Eircode is not long enough", e.getMessage());
		}
	}
	
//	Test No.	23
//	Test Objective 	To Verify that eircode of length 7 will return "Eircode is accepted"
//	Input(s)	Eircode:"N45K8G9"
//	Expected Output	Eircode is accepted
	
	public void testvalidateeircode004() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateeircode("N45K8G9");
			assertEquals("Eircode is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
		
//	Test No.	24
//	Test Objective	To Verify that eircode of length 8 will return "Eircode is accepted"
//	Input(s)	Eircode:"N45 K8G9"
//	Expected Output	Eircode is accepted
	
	public void testvalidateeircode005() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateeircode("N45 K8G9");
			assertEquals("Eircode is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			
			fail("Exception expected");
		}
	}
			
//	Test No. 	25
//	Test Objective	To Verify that eircode of length 9 will return "Eircode is too long"
//	Input(s)	Eircode: "N45K86G98"
//	Expected Output	Eircode is invalid character length is too long 
	
	public void testvalidateeircode006() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateeircode("N45K86G98");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Eircode is too long", e.getMessage());
		}
	}
			
//	Test No.	26
//	Test Objective	To Verify that eircode of length 15 will return "Eircode is too long"
//	Input(s)	Eircode:"N458K8657G98742"
//	Expected Output	Eircode is invalid character length is too long
	
	public void testvalidateeircode007() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateeircode("N458K8657G98742");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Eircode is too long", e.getMessage());
		}
	}

//	Test No.	27
//	Test Objective	To Verify that Item name of length 0 will return "Item name empty"
//	Input(s)	Item name:" "
//	Expected Output	Item name is invalid, no value entered
	
	public void testvalidateitemname001() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemname("");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item name has not been entered", e.getMessage());
		}
	}
	
//	Test No.	28
//	Test Objective	To Verify that Item name of length 1 will return "Item name is too short"
//	Input(s)	Item name:"K"
//	Expected Output	Item name character length is too short
	
	public void testvalidateitemname002() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemname("K");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item name is not long enough", e.getMessage());
		}
	}
	
//	Test No.	29
//	Test Objective	To Verify that Item name of length 5 will return "Item name is too short"
//	Input(s)	Item name:"KJFGL"
//	Expected Output	Item name character length is too short
	
	public void testvalidateitemname003() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemname("KJFGL");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item name is not long enough", e.getMessage());
		}
	}
	
//	Test No.	30
//	Test Objective 	To Verify that Item name of length 6 will return "Item name is accepted"
//	Input(s)	Item name: "KJFGLK"
//	Expected Output	Item name is accepted
	
	public void testvalidateitemname004() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateitemname("KJFGLK");
			assertEquals("Item name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No.	31
//	Test Objective	To Verify that Item name of length 21 will return "Item name is accepted"
//	Input(s)	Item name:"KJFGLKJGFLJGHTYGJREDF"
//	Expected Output	Item name is accepted"
	
	public void testvalidateitemname005() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateitemname("KJFGLKJGFLJGHTYGJREDF");
			assertEquals("Item name is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No.	32
//	Test Objective	To Verify that item name of length 22 will return "Item name is too long"
//	Input(s)	Item name:"KJFGLKJGHTYFORNSBDEGTH"
//	Expected Output	Item name character length is too long
	
	public void testvalidateitemname006() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemname("KJFGLKJGHTYFORNSBDEGTH");
			fail("Ecxeption expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item name is too long", e.getMessage());
		}
	}
		
//	Test No.	33
//	Test Objective 	To Verify that item name of length 50 will return "Item name is too long"
//	Input(s)	Item name:"KJGHLTYOUIDNCBVFTHENDVGHJTNRVFDVKGLHOUTGFHVNGHRFJGBG"
//	Expected Output	Item name character length is too long
	
	public void testvalidateitemname007() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemname("KJGHLTYOUIDNCBVFTHENDVGHJTNRVFDVKGLHOUTGFHVNGHRFJGBG");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item name is too long", e.getMessage());
		}
	}
		
//	Test No.	34
//	Test Objective	To Verify that Item description of length 0 will return "Item description empty"
//	Input(s)	Item description: " "
//	Expected Output	Item description is invalid, no value entered
	
	public void testvalidateitemdescription001() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemdescription("");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item description has not been entered", e.getMessage());
		}
	}
	
//	Test No.	35
//	Test Objective	To Verify that item description of length 1 will return "Item description is too short"
//	Input(s)	Item description: "K"
//	Expected Output	Item description is invalid character length is too short
	
	public void testvalidateitemdescription002() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemdescription("K");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item description is not long enough", e.getMessage());
		}
	}
	
//	Test No.	36
//	Test Objective	To Verify that item description of length 99 will return "Item description is to short"
//	Input(s)	Item description:*description with a length of 99 characters*
//	Expected Output	Item description is invalid character length is too short
	
	public void testvalidateitemdescription003() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemdescription("abcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwx");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item description is not long enough", e.getMessage());
		}
	}
	
//	Test No.	37
//	Test Objective	To Verify that item description of length 100 will return "item description is accepted"
//	Input(s)	Item description: *description with a length of 100 characters*
//	Expected Output	Item description is accepted
	
	public void testvalidateitemdescription004() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateitemdescription("abcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxy");
			assertEquals("Item description is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No.	38
//	Test Objective	To Verify that item description of length 200 will return"item description is accepted"
//	Input(s)	Item description:*description with a length of 200 characters*
//	Expected Output	Item description is accepted
	
	public void testvalidateitemdescription005() throws DeliverydocketExceptionHandler{
		
		try {
			
			String result = Deliverydocket.validateitemdescription("abcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxy");
			assertEquals("Item description is valid", result);
			
		}catch(DeliverydocketExceptionHandler e) {
			fail("Exception expected");
		}
	}
	
//	Test No.	39
//	Test Objective	To Verify that item description of length 201 will return "item description is too long"
//	Input(s)	Item description: *description with a length of 201 characters*
//	Expected Output	Item description is invalid character length is too long 
	
	public void testvalidateitemdescription006() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemdescription("aabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxy");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item description is too long", e.getMessage());
		}
	}
	
//	Test No.	40
//	Test Objective	To verify that item description of length 300 will return "item description is too long"
//	Input(s)	Item description: *description with a length of 300 characters*
//	Expected Output	Item description is invalid character length is too long
	
	public void testvalidateitemdescription007() throws DeliverydocketExceptionHandler{
		
		try {
			
			Deliverydocket.validateitemdescription("abcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxy");
			fail("Exception expected");
			
		}catch(DeliverydocketExceptionHandler e) {
			assertEquals("Item description is too long", e.getMessage());
		}
	}
	
}
