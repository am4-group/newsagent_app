import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class CommandLine {

	public static void main(String[] args) {
		try {
			// Create the Database Object
			Database dbObj = new Database();

			// Configure System for Running
			Scanner input = new Scanner(System.in);
			String choiceNumber = "-99";
			boolean keepAppOpen = true;
			boolean loggedIn = false;

			while (keepAppOpen == true) {

				while (loggedIn == false) {
					try {
						System.out.println("Choose an option:");
						System.out.println("1. Login");
						System.out.println("2. Register");
						choiceNumber = input.next();
						// Present options and wait for user choice
						if (choiceNumber.equals("1")) {
							System.out.print("Enter your username: ");
							String username = input.next();
							System.out.print("Enter your password: ");
							String password = input.next();

							boolean insertResult = dbObj.verifyAccount(username, password);

							if (insertResult == true) {
								System.out.println();
								System.out.println("Logged in successfully");
								loggedIn = true;
							} else {
								System.out.println();
								System.out.println("ERROR: Details incorrect");
								System.out.println();
							}

						} else if (choiceNumber.equals("2")) {
							System.out.print("Enter username: ");
							String username = input.next();
							System.out.print("Enter password: ");
							String password = input.next();
							System.out.println(username + " " + password);

							Account accObj = new Account(username, password);
							System.out.println(accObj.getUsername() + " " + accObj.getPassword());

							boolean insertResult = dbObj.insertAccountDetails(accObj);

							if (insertResult == true) {
								System.out.println();
								System.out.println("Account Details Saved");
								System.out.println();
							} else {
								System.out.println();
								System.out.println("ERROR: Account NOT Saved");
								System.out.println();
							}

						} else {
							System.out.println("Invalid option");
						}

					} catch (AccountExceptionHandler e) {
						System.out.println(e);
						System.out.println();
					}

				}

				listSystemOptions();
				choiceNumber = input.next();

				switch (choiceNumber) {
				case "1":
					customerMenu(dbObj);
					break;

				case "3":
					publicationMenu(dbObj);
					break;

				case "4":
					invoiceMenu(dbObj);
					break;

				case "5":
					deliverypersonMenu(dbObj);
					break;

				case "6":
					deliverydocketMenu(dbObj);
					break;

				case "7":
					deliveryareaMenu(dbObj);
					break;

				case "99":
					keepAppOpen = false;
					break;
				default:
					System.out.println("Invalid option");
					break;
				} // End system switch

			} // End while

			// Close scanner to free up resources
			input.close();
		}

		catch (Exception e) {
			System.out.println("PROGRAM TERMINATED - ERROR MESSAGE: " + e.getMessage());
		} // End try-catch

	} // End main

	private static void listSystemOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Customer Options");
		System.out.println("3. Publication Options");
		System.out.println("4. Invoice Options");
		System.out.println("5. Delivery Person Options");
		System.out.println("6. Delivery Docket Options");
		System.out.println("7. Delivery Area Options");
		System.out.println("99. Quit");
		System.out.println();
	}

	private static void listCustomerOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Customer Account");
		System.out.println("2. Modify Customer Account");
		System.out.println("3. View All Customer Accounts");
		System.out.println("4. Delete Customer Account");
		System.out.println("5. Archive Customer Account");
		System.out.println("6. Modify Customer Status");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void customerMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String cusChoiceNumber = "-99";

			while (cusChoiceNumber != "99") {
				// Present list of functionality and get selection
				listCustomerOptions();
				cusChoiceNumber = input.next();

				switch (cusChoiceNumber) {
				case "1":
					// Get Customer Details
					System.out.printf("Enter customer name: \n");
					String cusName = input.next();
					System.out.printf("Enter customer surname: \n");
					String cusSurname = input.next();
					System.out.printf("Enter customer phone number: \n");
					String cusphoneNumber = input.next();
					System.out.printf("Enter customer eircode: \n");
					String cusEircode = input.next();
					System.out.printf("Enter customer age: \n");
					int cusAge = input.nextInt();
					System.out.printf("Enter customer delivery area: \n");
					String cusDeliveryArea = input.next();

					// Create new customer object
					Customer cusObj = new Customer(cusName, cusSurname, cusphoneNumber, cusEircode, cusAge,
							cusDeliveryArea, "Home");

					// Insert Customer Details into the database
					boolean insertResult = dbObj.insertCustomerDetails(cusObj);

					if (insertResult == true)
						System.out.println("Customer Details Saved");
					else
						System.out.println("ERROR: Customer Details NOT Saved");
					break;

				case "2":
					// Modify customer account by ID
					// Get Customer Details
					System.out.printf("Enter customer ID: \n");
					int modifyCustId = input.nextInt();
					System.out.printf("Enter customer name: \n");
					String modifyCusName = input.next();
					System.out.printf("Enter customer surname: \n");
					String modifyCusSurname = input.next();
					System.out.printf("Enter customer phone number: \n");
					String modifyCusphoneNumber = input.next();
					System.out.printf("Enter customer eircode: \n");
					String modifyCusEircode = input.next();
					System.out.printf("Enter customer age: \n");
					int modifyCusAge = input.nextInt();
					System.out.printf("Enter customer delivery area: \n");
					String modifyCusDeliveryArea = input.next();

					boolean modifyResult = dbObj.modifyCustomerById(modifyCustId, modifyCusName, modifyCusSurname,
							modifyCusphoneNumber, modifyCusEircode, modifyCusAge, modifyCusDeliveryArea);

					if (modifyResult == true)
						System.out.println("Customer successfully modified");
					else
						System.out.println("ERROR: Customer Details NOT modified or Do Not Exist");
					break;

				case "3":
					// Retrieve ALL Customer Records
					ResultSet rSet = dbObj.viewAllCustomers();

					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					} else {
						boolean tablePrinted = printCustomerTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}

					break;

				case "4":
					// Delete Customer Record by ID
					System.out.println("Enter Customer ID to be deleted or -99 to delete all rows");
					int deleteCustId = input.nextInt();

					boolean deleteResult = dbObj.deleteCustomerById(deleteCustId);

					if ((deleteResult == true) && (deleteCustId == -99))
						System.out.println("Customer table emptied");
					else if (deleteResult == true)
						System.out.println("Customer with ID " + deleteCustId + " deleted");
					else
						System.out.println("ERROR: Customer Details NOT Deleted or Do Not Exist");
					break;

				case "5":
					// Archive customer account
					System.out.println("Enter Customer ID to be archived or -99 to archive all rows");
					int archiveCustId = input.nextInt();

					boolean archiveCustIdResult = dbObj.archiveCustomerByID(archiveCustId);

					if ((archiveCustIdResult == true) && (archiveCustId == -99))
						System.out.println("Customer table archived");
					else if (archiveCustIdResult == true)
						System.out.println("Customer with ID " + archiveCustId + " archived");
					else
						System.out.println("ERROR: Customer Details NOT archived or Do Not Exist");
					break;

				case "6":
					System.out.println("Enter Customer ID to change the status");
					int modifyStatusCustId = input.nextInt();
					System.out.println("Choose status:\n1. Home\n2. Away");
					int statusChoice = input.nextInt();

					String status = "";
					if (statusChoice == 1) {
						status = "Home";
					} else if (statusChoice == 2) {
						status = "Away";
					} else {
						System.out.println("Invalid option");
						break;
					}

					boolean modifyStatusResult = dbObj.modifyCustomerStatusById(modifyStatusCustId, status);

					if (modifyStatusResult == true)
						System.out.println("Customer status successfully modified");
					else
						System.out.println("ERROR: Customer status NOT modified or Do Not Exist");
					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End customer switch

			}
			input.close();
		} catch (

		Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printCustomerTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full Customer Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("customer_id");
			String name = rs.getString("firstName");
			String addr = rs.getString("lastName");
			String phone = rs.getString("phone");
			String eircode = rs.getString("custEircode");
			String age = rs.getString("age");
			String delivery_area = rs.getString("deliveryArea");
			String status = rs.getString("atHomeStatus");
			System.out.printf("%30s", id);
			System.out.printf("%30s", name);
			System.out.printf("%30s", addr);
			System.out.printf("%30s", phone);
			System.out.printf("%30s", eircode);
			System.out.printf("%30s", age);
			System.out.printf("%30s", delivery_area);
			System.out.printf("%30s", status);
			System.out.println();
		} // end while
		System.out.println("------------------------------");

		return true;
	}

	// <Publication---------------------------------------------------------------------------------->

	private static void listPublicationOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Publication");
		System.out.println("2. Modify Publication");
		System.out.println("3. View All Publications");
		System.out.println("4. Delete Publication");
		System.out.println("5. Archive Publication");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void publicationMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String pubChoiceNumber = "-99";

			while (pubChoiceNumber != "99") {
				// Present list of functionality and get selection
				listPublicationOptions();
				pubChoiceNumber = input.next();

				switch (pubChoiceNumber) {
				case "1":
					// Get Publication Details
					System.out.printf("Enter publication name: \n");
					String pubName = input.next();
					System.out.printf("Enter publication price: \n");
					double pubPrice = input.nextDouble();
					System.out.printf("Enter publication description: \n");
					String pubDescription = input.next();
					System.out.printf("Enter publication frequency: \n");
					String pubFrequency = input.next();

					// Create new publication object
					Publication pubObj = new Publication(pubName, pubPrice, pubDescription, pubFrequency);

					// Insert Publication Details into the database
					boolean insertResult = dbObj.insertPublication(pubObj);

					if (insertResult == true)
						System.out.println("Publication Details Saved");
					else
						System.out.println("ERROR: Publication Details NOT Saved");

				case "2":
					// Modify Publication account by ID
					// Get Publication Details
					System.out.printf("Enter publication id: \n");
					int ModifyPubId = input.nextInt();
					System.out.printf("Enter publication name: \n");
					String ModifyPubName = input.next();
					System.out.printf("Enter publication price: \n");
					double ModifyPubPrice = input.nextDouble();
					System.out.printf("Enter publication description: \n");
					String ModifyPubDescription = input.next();
					System.out.printf("Enter publication frequency: \n");
					String ModifyPubFrequency = input.next();

					boolean modifyResult = dbObj.modifyPublicationByID(ModifyPubId, ModifyPubName, ModifyPubPrice,
							ModifyPubDescription, ModifyPubFrequency);

					if (modifyResult == true)
						System.out.println("Publication successfully modified");
					else
						System.out.println("ERROR: Publication Details NOT modified or Do Not Exist");
					break;

				case "3":
					// Retrieve ALL Publication Records
					ResultSet rSet = dbObj.viewAllPublications();

					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					} else {
						boolean tablePrinted = printPublicationTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}
					break;

				case "4":// delete
					// Delete Publication Record by ID
					System.out.println("Enter Publication ID to be deleted or -99 to delete all rows");
					int deletePubId = input.nextInt();

					boolean deleteResult = dbObj.deletePublicationrById(deletePubId);

					if ((deleteResult == true) && (deletePubId == -99))
						System.out.println("Publication table emptied");
					else if (deleteResult == true)
						System.out.println("Publication with ID " + deletePubId + " deleted");
					else
						System.out.println("ERROR: Publication Details NOT Deleted or Do Not Exist");
					break;

				case "5":// archive
					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End publication switch

			}
			input.close();
		} catch (Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printPublicationTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full Publication Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("publication_id");
			String pubname = rs.getString("pub_name");
			String pubprice = rs.getString("price");
			String pubdesc = rs.getString("pub_description");
			String pubfreq = rs.getString("frequency");
			System.out.printf("%30s", id);
			System.out.printf("%30s", pubname);
			System.out.printf("%30s", pubprice);
			System.out.printf("%30s", pubdesc);
			System.out.printf("%30s", pubfreq);
			System.out.println();
		} // end while
		System.out.println("------------------------------");

		return true;
	}

	// <Invoice---------------------------------------------------------------------------------->

	private static void listInvoiceOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Invoice");
		System.out.println("2. Modify Invoice");
		System.out.println("3. View All Invoices");
		System.out.println("4. Delete Invoice");
		System.out.println("5. Archive Invoice");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void invoiceMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String invChoiceNumber = "-99";

			while (invChoiceNumber != "99") {
				// Present list of functionality and get selection
				listInvoiceOptions();
				invChoiceNumber = input.next();

				switch (invChoiceNumber) {
				case "1":
					// Get Invoice Details

					// Get Customer Details
					System.out.printf("Enter customer name: \n");
					String cusName = input.next();
					System.out.printf("Enter customer surname: \n");
					String cusSurname = input.next();
					System.out.printf("Enter customer phone number: \n");
					String cusphoneNumber = input.next();
					System.out.printf("Enter customer eircode: \n");
					String cusEircode = input.next();
					System.out.printf("Enter customer age: \n");
					int cusAge = input.nextInt();
					System.out.printf("Enter customer delivery area: \n");
					String cusDeliveryArea = input.next();

					// Create new customer object
					Customer cusObj = new Customer(cusName, cusSurname, cusphoneNumber, cusEircode, cusAge,
							cusDeliveryArea, "Home");

					System.out.printf("Enter company name: \n");
					String companyName = input.next();
					System.out.printf("Enter company eircode: \n");
					String companyEircode = input.next();

					// Get Publication Details
					System.out.printf("Enter publication name: \n");
					String pubName = input.next();
					System.out.printf("Enter publication price: \n");
					double pubPrice = input.nextDouble();
					System.out.printf("Enter publication description: \n");
					String pubDescription = input.next();
					System.out.printf("Enter publication frequency: \n");
					String pubFrequency = input.next();

					// Create new publication object
					Publication pubObj = new Publication(pubName, pubPrice, pubDescription, pubFrequency);

					System.out.printf("Enter total price: \n");
					double totalPrice = input.nextDouble();

					// Create new Invoice object

					Date date = new Date();
					SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
					String stringDate = DateFor.format(date);

					Invoice invObj = new Invoice(cusObj, companyName, companyEircode, pubObj, totalPrice, stringDate);

					// Insert Invoice Details into the database
					boolean insertResult = dbObj.insertInvoice(invObj);

					if (insertResult == true)
						System.out.println("Invoice Details Saved");
					else
						System.out.println("ERROR: Invoice Details NOT Saved");

					break;

				case "2":
					// Modify Invoice account by ID
					// Get Invoice Details
					System.out.printf("Enter invoice id: \n");
					int ModifyInvId = input.nextInt();
					System.out.printf("Enter customer first name: \n");
					String ModifycusName = input.next();
					System.out.printf("Enter customer last name: \n");
					String ModifycusSurname = input.next();
					System.out.printf("Enter customer eircode: \n");
					String ModifycusEircode = input.next();

					System.out.printf("Enter company name: \n");
					String ModifycompanyName = input.next();
					System.out.printf("Enter company eircode: \n");
					String ModifycompanyEircode = input.next();

					System.out.printf("Enter publication name: \n");
					String ModifypubName = input.next();

					System.out.printf("Enter total price: \n");
					double ModifytotalPrice = input.nextDouble();

					Date date2 = new Date();
					SimpleDateFormat DateFor2 = new SimpleDateFormat("dd/MM/yyyy");
					String stringDate2 = DateFor2.format(date2);

					boolean modifyResult = dbObj.modifyInvoiceByID(ModifyInvId, ModifycusName, ModifycusSurname,
							ModifycusEircode, ModifycompanyName, ModifycompanyEircode, ModifypubName, ModifytotalPrice,
							stringDate2);

					if (modifyResult == true)
						System.out.println("Invoice successfully modified");
					else
						System.out.println("ERROR: Invoice Details NOT modified or Do Not Exist");
					break;

				case "3":
					// Retrieve ALL Invoice Records
					ResultSet rSet = dbObj.viewAllInvoices();

					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					} else {
						boolean tablePrinted = printInvoiceTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}
					break;

				case "4":// delete invoice
							// Delete invoice Record by ID
					System.out.println("Enter the invoice ID to be deleted or -99 to delete all rows");
					int deleteINV = input.nextInt();

					boolean deleteResult = dbObj.deleteDeliveryDocketById(deleteINV);

					if ((deleteResult == true) && (deleteINV == -99)) {
						System.out.println("Invoice table emptied");
					} else if (deleteResult == true) {
						System.out.println("Invoice with ID " + deleteINV + " deleted");
					} else {
						System.out.println("ERROR: Invoice Details NOT Deleted or Do Not Exist");
					}
					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End Invoice switch

			}
			input.close();
		} catch (Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printInvoiceTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full Invoice Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("invoice_id");
			String customerName = rs.getString("customerName");
			String customerSurname = rs.getString("customerName");
			String customerEircode = rs.getString("customerEircode");
			String companyName = rs.getString("customerName");
			String companyEircode = rs.getString("company_address");
			String pubName = rs.getString("publicationName");
			double tprice = rs.getDouble("price");
			String date = rs.getString("currentDate");

			System.out.printf("%30s", id);
			System.out.printf("%30s", customerName);
			System.out.printf("%30s", customerSurname);
			System.out.printf("%30s", customerEircode);
			System.out.printf("%30s", companyName);
			System.out.printf("%30s", companyEircode);
			System.out.printf("%30s", pubName);
			System.out.printf("%30s", tprice);
			System.out.printf("%30s", date);
			System.out.println();
		} // end while
		System.out.println("------------------------------");

		return true;
	}
	// <Deliverydocket---------------------------------------------------------------------------------->

	private static void listDeliverydocketOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Delivery Docket");
		System.out.println("2. Modify Delivery Docket");
		System.out.println("3. View All Delivery Docket");
		System.out.println("4. Delete Delivery Docket");
		System.out.println("5. Archive Delivery Docket");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void deliverydocketMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String delChoiceNumber = "-99";

			while (delChoiceNumber != "99") {
				// Present list of functionality and get selection
				listDeliverydocketOptions();
				delChoiceNumber = input.next();

				switch (delChoiceNumber) {
				case "1":
					// Get Deliverydocket Details
					System.out.printf("Enter delivery first name: \n");
					String delName = input.next();
					System.out.printf("Enter delivery last name: \n");
					String dellastName = input.next();
					System.out.printf("Enter delivery eircode: \n");
					String delEircode = input.next();
					System.out.printf("Enter delivery item name: \n");
					String delitemName = input.next();
					System.out.printf("Enter delivery item description: \n");
					String delitemDescription = input.next();

					// Create new deliverydocket object
					Deliverydocket delObj = new Deliverydocket(delName, dellastName, delEircode, delitemName,
							delitemDescription);

					// Insert Deliverydocket Details into the database
					boolean insertResult = dbObj.insertDeliveryDocket(delObj);

					if (insertResult == true)
						System.out.println("DeliveryDocket Details Saved");
					else
						System.out.println("ERROR: DeliveryDocket Details NOT Saved");

				case "2":
					// Modify DeliveryDocket by ID
					// Get DeliveryDocket Details
					System.out.printf("Enter delivery ID");
					int ModifyDelID = input.nextInt();
					System.out.printf("Enter delivery first name: \n");
					String ModifyDelName = input.next();
					System.out.printf("Enter delivery last name: \n");
					String ModifyDellastName = input.next();
					System.out.printf("Enter delivery eircode: \n");
					String ModifyDelEircode = input.next();
					System.out.printf("Enter delivery item name: \n");
					String ModifydelitemName = input.next();
					System.out.printf("Enter delivery item description: \n");
					String ModifydelitemDescription = input.next();

					boolean modifyResult = dbObj.modifyDeliveryDocket(ModifyDelID, ModifyDelName, ModifyDellastName,
							ModifyDelEircode, ModifydelitemName, ModifydelitemDescription);

					if (modifyResult == true)
						System.out.println("DeliveryDocket successfully modified");
					else
						System.out.println("ERROR: DeliveryDocket Details NOT modified or Do Not Exist");
					break;

				case "3":
					// Retrieve ALL Deliverydocket Records
					ResultSet rSet = dbObj.viewAllDeliveryDocket();

					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					} else {
						boolean tablePrinted = printDeliverydocketTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}
					break;

				case "4":// delete
					// Delete DeliveryDocket Record by ID
					System.out.println("Enter DeliveryDocket ID to be deleted or -99 to delete all rows");
					int deleteDelId = input.nextInt();

					boolean deleteResult = dbObj.deleteDeliveryDocketById(deleteDelId);

					if ((deleteResult == true) && (deleteDelId == -99))
						System.out.println("DeliveryDocket table emptied");
					else if (deleteResult == true)
						System.out.println("DeliveryDocket with ID " + deleteDelId + " deleted");
					else
						System.out.println("ERROR: DeliveryDocket Details NOT Deleted or Do Not Exist");
					break;

				case "5":// archive

					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End DeliveryDocket switch

			}
			input.close();
		} catch (Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printDeliverydocketTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full Publication Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("delDocketID");
			String delname = rs.getString("fName");
			String dellastname = rs.getString("lastName");
			String deleircode = rs.getString("eircode");
			String delitemname = rs.getString("itemName");
			String delitemdescription = rs.getString("itemName");
			System.out.printf("%30s", id);
			System.out.printf("%30s", delname);
			System.out.printf("%30s", dellastname);
			System.out.printf("%30s", deleircode);
			System.out.printf("%30s", delitemname);
			System.out.printf("%30s", delitemdescription);
			System.out.println();

		} // end while
		System.out.println("------------------------------");

		return true;
	}

// <Delivery Person>=========================================================================================

	private static void listDeliveryPersonOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Delivery Person Account");
		System.out.println("2. Modify Delivery Person Account");
		System.out.println("3. View All Delivery Person Accounts");
		System.out.println("4. Delete Delivery Person Account");
		System.out.println("5. Archive Delivery Person Account");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void deliverypersonMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String delpChoiceNumber = "-99";

			while (delpChoiceNumber != "99") {
				// Present list of functionality and get selection
				listDeliveryPersonOptions();
				delpChoiceNumber = input.next();

				switch (delpChoiceNumber) {
				case "1":
					// Get Delivery Person Details
					System.out.printf("Enter Delivery Person firt name: \n");
					String DelpFirstName = input.next();
					System.out.printf("Enter Delivery Person last name: \n");
					String DelpLastName = input.next();
					System.out.printf("Enter Delivery Person phone number: \n");
					String DelpPhoneNumber = input.next();
					System.out.printf("Enter Delivery Person delivery area: \n");
					String DelpDeliveryArea = input.next();

					// Create new Delivery Person object
					Deliveryperson delpObj = new Deliveryperson(DelpFirstName, DelpLastName, DelpPhoneNumber,
							DelpDeliveryArea);

					// insert Delivery Person Details into the database
					boolean insertResult = dbObj.insertDeliveryPerson(delpObj);

					if (insertResult == true) {
						Account accObj = new Account(DelpFirstName.toLowerCase(), DelpLastName.toLowerCase());
						dbObj.insertAccountDetails(accObj);
						System.out.println("Delivery Person Details Saved");
					} else {
						System.out.println("ERROR: Delivery Person Details NOT Saved");
					}
					break;

				case "2":
					// Modify Delivery Person account by ID
					// Get Delivery Person Details
					System.out.printf("Enter delivery person ID: \n");
					int modifyDelpId = input.nextInt();
					System.out.printf("Enter delivery person name: \n");
					String modifyDelpFirstName = input.next();
					System.out.printf("Enter delivery person surname: \n");
					String modifyDelpLastName = input.next();
					System.out.printf("Enter delivery person phone number: \n");
					String modifyDelpPhoneNumber = input.next();
					System.out.printf("Enter delivery person delivery area: \n");
					String modifyDelpDeliveryArea = input.next();

					boolean modifyResult = dbObj.modifyDeliveryPerson(modifyDelpId, modifyDelpFirstName,
							modifyDelpLastName, modifyDelpPhoneNumber, modifyDelpDeliveryArea);

					if (modifyResult == true)
						System.out.println("Delivery Person Details successfully modified");
					else
						System.out.println("ERROR: Delivery Person Details NOT modified or Do Not Exist");
					break;

				case "3":
					// Retrieve ALL Delivery Person Records
					ResultSet rSet = dbObj.viewAllDeliveryPerson();

					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					} else {
						boolean tablePrinted = printDeliveryPersonTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}
					break;

				case "4":
					// Delete Delivery Person Record by ID
					System.out.println("Enter Delivery Person ID to be deleted or -99 to delete all rows");
					int deleteDelpid = input.nextInt();

					boolean deleteResult = dbObj.deleteDeliveryPersonById(deleteDelpid);

					if ((deleteResult == true) && (deleteDelpid == -99))
						System.out.println("Delivery Person table emptied");
					else if (deleteResult == true)
						System.out.println("Delivery Person with ID " + deleteDelpid + " deleted");
					else
						System.out.println("ERROR: Delivery Person Details NOT Deleted or Do Not Exist");
					break;

				case "5":
					// Archive Delivery Person account
					System.out.println("Enter Delivery Person ID to be archived or -99 to archive all rows");
					int archiveDelpId = input.nextInt();

//					boolean archiveDelpId = dbObj.archiveDeliveryPersonById(archiveDelpId);
					//
//					if ((archiveDelpId == true) && (archiveDelpId == -99))
//						System.out.println("Delivery Person table archived");
//					else if (archiveCustId == true)
//						System.out.println("Delivery Person with ID " + archiveDelpId + " archived");
//					else
//						System.out.println("ERROR: Delivery Person Details NOT archived or Do Not Exist");
					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End Delivery Person switch

			}
			input.close();
		} catch (Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printDeliveryPersonTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full Delivery Person Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("delivery_person_id");
			String firstname = rs.getString("firstName");
			String lastname = rs.getString("lastName");
			String phone_number = rs.getString("phonenumber");
			String delivery_area = rs.getString("deliveryArea");
			System.out.printf("%30s", id);
			System.out.printf("%30s", firstname);
			System.out.printf("%30s", lastname);
			System.out.printf("%30s", phone_number);
			System.out.printf("%30s", delivery_area);
			System.out.println();

		} // end while
		System.out.println("------------------------------");

		return true;
	}

	// <Deliveryarea---------------------------------------------------------------------------------->

	private static void listDeliveryareaOptions() {
		System.out.println();
		System.out.println("Choose an option:");
		System.out.println("1. Create Delivery Area");
		System.out.println("2. Modify Delivery Area");
		System.out.println("3. View All Delivery Area");
		System.out.println("4. Delete Delivery Area");
		System.out.println("5. Archive Delivery Area");
		System.out.println("99. Back");
		System.out.println();
	}

	private static void deliveryareaMenu(Database dbObj) {
		try {
			Scanner input = new Scanner(System.in);
			String delareaChoiceNumber = "-99";

			while (delareaChoiceNumber != "99") {
				// Present list of functionality and get selection
				listDeliveryareaOptions();
				delareaChoiceNumber = input.next();

				switch (delareaChoiceNumber) {
				case "1":
					// Get Deliveryarea Details
					System.out.printf("Enter deliveryarea name: \n");
					String delareaName = input.next();
					System.out.printf("Enter deliveryarea routingkey: \n");
					String delroutingkey = input.next();

					// Create new deliveryarea object
					Deliveryarea delObj = new Deliveryarea(delareaName, delroutingkey);

					// Insert Deliveryarea Details into the database
					boolean insertResult = dbObj.insertDeliveryArea(delObj);

					if (insertResult == true)
						System.out.println("DeliveryArea Details Saved");
					else
						System.out.println("ERROR: DeliveryArea Details NOT Saved");

				case "2":
					// Modify DeliveryArea by ID
					// Get DeliveryArea Details
					System.out.printf("Enter deliveryarea id:");
					int ModifyDelareaid = input.nextInt();
					System.out.printf("Enter deliveryarea name: \n");
					String ModifyDelareaname = input.next();
					System.out.printf("Enter deliveryarea routingkey: \n");
					String Modifyroutingkey = input.next();

					boolean modifyResult = dbObj.modifyDeliveryArea(ModifyDelareaid, ModifyDelareaname,
							Modifyroutingkey);

					if (modifyResult == true)
						System.out.println("DeliveryArea successfully modified");
					else
						System.out.println("ERROR: DeliveryArea Details NOT modified or Do Not Exist");
					break;

				case "3":// view

					break;

				case "4":// delete
					// Delete DeliveryArea Record by ID
					System.out.println("Enter DeliveryArea ID to be deleted or -99 to delete all rows");
					int deletedelAreaId = input.nextInt();

					boolean deleteResult = dbObj.deleteDeliveryArearById(deletedelAreaId);

					if ((deleteResult == true) && (deletedelAreaId == -99))
						System.out.println("DeliveryDocket table emptied");
					else if (deleteResult == true)
						System.out.println("DeliveryArea with ID " + deletedelAreaId + " deleted");
					else
						System.out.println("ERROR: DeliveryArea Details NOT Deleted or Do Not Exist");
					break;

				case "5":// archive

					break;

				case "99":
					// Back
					return;

				default:
					System.out.println("Invalid option");
					break;
				} // End DeliveryArea switch

			}
			input.close();
		} catch (Exception e) {
			System.out.println("ERROR MESSAGE: " + e.getMessage());
		} // End try-catch
	}

	private static boolean printDeliveryareaTable(ResultSet rs) throws Exception {
		// Print The Contents of the Full DeliveryArea Table
		System.out.println("------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));

		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%30s", rs.getMetaData().getColumnName(i));
		}

		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("delAreaID");
			String delareaname = rs.getString("delAreaName");
			String delroutingkey = rs.getString("routingKey");
			System.out.printf("%30s", id);
			System.out.printf("%30s", delareaname);
			System.out.printf("%30s", delroutingkey);
			System.out.println();

		} // end while
		System.out.println("------------------------------");

		return true;
	}
}