
public class Customer {
	private int id;
	private String name;
	private String surname;
	private String phoneNumber;
	private String eircode;
	private Integer age;
	private String deliveryArea;
	private String atHomeStatus;

	public Customer() throws CustomerExceptionHandler {
		throw new RuntimeException("No code written");
	}

	public Customer(String name, String surname, String phoneNumber, String eircode, Integer age, String deliveryArea,
			String status) throws CustomerExceptionHandler {
		id = 0;

		// Validate Input
		try {

			validateName(name);
			validateSurname(surname);
			validatePhoneNumber(phoneNumber);
			validateEircode(eircode);
			validateAge(age);
			validateDeliveryArea(deliveryArea);

		} catch (CustomerExceptionHandler e) {
			throw e;
		}

		this.name = name;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
		this.eircode = eircode;
		this.age = age;
		this.deliveryArea = deliveryArea;
		this.atHomeStatus = status;
	}

	public static String validateName(String name) throws CustomerExceptionHandler {
		if (name.isBlank() || name.isEmpty())
			throw new CustomerExceptionHandler("Customer name not entered");
		else if (name.length() < 2)
			throw new CustomerExceptionHandler("Customer name does not meet minimum length requirements");
		else if (name.length() > 30)
			throw new CustomerExceptionHandler("Customer name exceeds maximum length requirements");
		else
			return "Customer name valid";
	}

	public static String validateSurname(String surname) throws CustomerExceptionHandler {
		if (surname.isBlank() || surname.isEmpty())
			throw new CustomerExceptionHandler("Customer surname not entered");
		else if (surname.length() < 2)
			throw new CustomerExceptionHandler("Customer surname does not meet minimum length requirements");
		else if (surname.length() > 30)
			throw new CustomerExceptionHandler("Customer surname exceeds maximum length requirements");
		else
			return "Customer surname valid";
	}

	public static String validatePhoneNumber(String phoneNumber) throws CustomerExceptionHandler {
		if (phoneNumber.isBlank() || phoneNumber.isEmpty())
			throw new CustomerExceptionHandler("Customer phone number not entered");
		else if (phoneNumber.length() < 10)
			throw new CustomerExceptionHandler("Customer phone number does not meet minimum length requirements");
		else if (phoneNumber.length() > 10)
			throw new CustomerExceptionHandler("Customer phone number exceeds maximum length requirements");
		else
			return "Customer phone number valid";
	}

	public static String validateEircode(String eircode) throws CustomerExceptionHandler {
		if (eircode.isBlank() || eircode.isEmpty())
			throw new CustomerExceptionHandler("Customer eircode not entered");
		else if (eircode.length() < 7)
			throw new CustomerExceptionHandler("Customer eircode does not meet minimum length requirements");
		else if (eircode.length() > 8)
			throw new CustomerExceptionHandler("Customer eircode exceeds maximum length requirements");
		else
			return "Customer eircode valid";
	}

	public static String validateAge(Integer age) throws CustomerExceptionHandler {
		if (age == null)
			throw new CustomerExceptionHandler("Customer age not entered");
		else if (age < 18)
			throw new CustomerExceptionHandler("Customer age does not meet minimum length requirements");
		else
			return "Customer age valid";
	}

	public static String validateDeliveryArea(String deliveryArea) throws CustomerExceptionHandler {
		if (deliveryArea.isBlank() || deliveryArea.isEmpty()) {
			throw new CustomerExceptionHandler("Customer delivery area not entered");
		} else if (deliveryArea.length() < 3) {
			throw new CustomerExceptionHandler("Customer delivery area does not meet minimum length requirements");
		} else if (deliveryArea.length() > 3) {
			throw new CustomerExceptionHandler("Customer delivery area exceeds maximum length requirements");
		} else {
			return "Customer delivery area valid";
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEircode() {
		return eircode;
	}

	public void setEircode(String eircode) {
		this.eircode = eircode;
	}

	public int getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAtHomeStatus() {
		return atHomeStatus;
	}

	public void setAtHomeStatus(String atHomeStatus) {
		this.atHomeStatus = atHomeStatus;
	}

}