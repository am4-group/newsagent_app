import junit.framework.TestCase;

public class DeliveryareaTest extends TestCase {
	
//	Test No.	1
//	Test Objective	To Verify that an Area name of length 0 will return "Area name empty"
//	Test Type	Junit
//	Input(s)	Area name: ""
//	Expected Output	Area name is invalid, no value entered

	public void testvalidateareaname001() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateareaname("");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Area name is invalid, no value entered", e.getMessage());
		}
	}
//	Test No.	2
//	Test Objective	To Verify that an Area name of length 1 will return "Area name is too short"
//	Test Type	Junit
//	Input(s)	Area name: "A"
//	Expected Output	Area name character length is too short
	
	public void testvalidateareaname002() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateareaname("A");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Area name character length is too short", e.getMessage());
		}
	}
	
//	Test No.	3
//	Test Objective	To Verify that an Area name of length 2 will return "Area name is too short"
//	Test Type	Junit
//	Input(s)	Area name:"AA"
//	Expected Output	Area name character length is too short
	
	public void testvalidateareaname003() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateareaname("AA");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Area name character length is too short", e.getMessage());
		}
	}
	
//	Test No.	4
//	Test Objective	To Verify that an Area name of length 3 will return "Area name is acceted"
//	Test Type	Junit
//	Input(s)	Area name:"AAA"
//	Expected Output	Area name is accepted
	
	public void testvalidateareaname004() throws DeliveryareaExceptionHandler{
		
		try {
			
			String result = Deliveryarea.validateareaname("AAA");
			assertEquals("Area name is accepted", result);
			
		}catch(DeliveryareaExceptionHandler e) {
			fail("Exception expected");
			
		}
	}

//	Test No.	5
//	Test Objective	To Verify that an Area name of length 4 will return "Area name is too long"
//	Test Type	Junit
//	Input(s)	Area name:"AAAA"
//	Expected Output	Area name is invalid character length is too long
	
	public void testvalidateareaname005() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateareaname("AAAA");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Area name is invalid character length is too long", e.getMessage());
		}
	}
	
//	Test No.	6
//	Test Objective	To Verify that a routing key of length 0 will return "routing key is empty"
//	Test Ytpe	Junit
//	Input(s)	Routing key:" "
//	Expected Output	Routing key is invalid, no value entered
	
	public void testvalidateroutingkey001() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateroutingkey("");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Routing key is invalid, no value entered", e.getMessage());
		}
	}
//	Test No.	7
//	Test Objective	To Verify that an routing key of length 1 will return "Routing key is too short"
//	Test Type	Junit
//	Input(s)	Area name: "A"
//	Expected Output	Routing key charater length is too short

	public void testvalidateroutingkey002() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateroutingkey("A");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Routing key character length is too short", e.getMessage());
		}
	}
//	Test No.	8
//	Test Objective	To Verify that an Routing key of length 2 will return "Routing key is too short"
//	Test Type	Junit
//	Input(s)	Area name:"AA"
//	Expected Output	Routing key charater length is too short

	public void testvalidateroutingkey003() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateroutingkey("AA");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Routing key character length is too short", e.getMessage());
		}
	}
//	Test No.	4
//	Test Objective	To Verify that an Routing key of length 3 will return "Routing key is accepted"
//	Test Type	Junit
//	Input(s)	Area name:"AAA"
//	Expected Output	Routing key is accepted

	public void testvalidateroutingkey004() throws DeliveryareaExceptionHandler{
		
		try {
			
			String result = Deliveryarea.validateroutingkey("AAA");
			assertEquals("Routing key is accepted", result);
			
		}catch(DeliveryareaExceptionHandler e) {
			fail("Exception expected");
			
		}
	}
//	Test No.	5
//	Test Objective	To Verify that an Routing key of length 4 will return "Routing key is too long"
//	Test Type	Junit
//	Input(s)	Area name:"AAAA"
//	Expected Output	Routing key is invalid character length is too long

	public void testvalidateroutingkey005() throws DeliveryareaExceptionHandler{
		
		try {
			
			Deliveryarea.validateroutingkey("AAAA");
			fail("Exception expected");
			
		}catch(DeliveryareaExceptionHandler e) {
			assertEquals("Routing key is invalid character length is too long", e.getMessage());
		}
	}
	
//	Test No.	6
//	Test Objective	To Verify that multiple empty values will return an error
//	Test Type	Junit
//	Input(s)	Area name:" "
//		Routing key: " "
//	Expected Output	Area name is invalid, no value entered
//		Routing key is invalid, no value entered

 	public void testvalidateCreateDeliveryarea001() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("","");
 			
 			fail("Exception expected");
 		}catch (DeliveryareaExceptionHandler e) {

			assertEquals("Area name is invalid, no value entered", e.getMessage());
 		}
 	}
//	Test No.	7
//	Test Objective 	To test for the range of valid input values
//	Test Type 	Junit 
//	Input(s)	Area name:"WEN"
//		Routing Key:"N39"
//	Expected Output	TRUE

 	public void testvalidateCreateDeliveryarea002() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("WEN", "N39");
 			
 			assertEquals(0, testObj.getid());
 			assertEquals("WEN", testObj.getareaname());
 			assertEquals("N39", testObj.getroutingkey());
 			
 		}catch (DeliveryareaExceptionHandler e) {

 			fail("Exception expected");
 		}
 	}
//	Test No.	8
//	Test Objective	To test for the first range of invalid input values
//	Test Type	Junit 
//	Input(s)	Area name:"W"
//		Routing Key:"N"
//	Expected Output	Area name is invalid character length too short
//		Routing key is invalid character length too short

 	public void testvalidateCreateDeliveryarea003() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("W", "N");
 			
 			fail("Exception expected");
 		}catch (DeliveryareaExceptionHandler e) {

			assertEquals("Area name character length is too short", e.getMessage());
 		}
 	}
//	Test No. 	9
//	Test Objective 	To test for the second range of invalid input values 
//	Test Type	Junit
//	Input(s)	Area name:"WENS"
//		Routing Key:"N39k"
//	Expected Output	Area name is invalid character length too long
//		Routing key is invalid character length too long
 	
 	public void testvalidateCreateDeliveryarea004() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("WENS", "N39k");
 			
 			fail("Exception expected");
 		}catch (DeliveryareaExceptionHandler e) {

			assertEquals("Area name is invalid character length is too long", e.getMessage());
 		}
 	}

// 	Test No.	10
// 	Test Objective 	To test for the first range of both valid and invalid input values
// 	Test Type	Junit
// 	Input(s)	Area name: "N"
// 		Routing key: "N39"
// 	Expected Output	Area name is invalid character length too short

 	public void testvalidateCreateDeliveryarea005() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("N","N39");
 			
 			fail("Exception expected");
 		}catch (DeliveryareaExceptionHandler e) {
 			
 			assertEquals("Area name character length is too short", e.getMessage());
 		}
 	}
// 	Test No. 	11
// 	Test Objective	To test for the second range of both valid and invalid input values
// 	Test Type	Junit
// 	Input(s)	Area name: "WEN"
// 		Routing key:"N39K"
// 	Expected Output	Routing key is invalid character length too long

 	public void testvalidateCreateDeliveryarea006() throws DeliveryareaExceptionHandler{
 		
 		try {
 			Deliveryarea testObj = new Deliveryarea("WEN","N39K");
 			
 			fail("Exception expected");
 		}catch (DeliveryareaExceptionHandler e) {
 			
 			assertEquals("Routing key is invalid character length is too long", e.getMessage());
 		}
 	}
	

}
