
public class Publication {

	private int pubId;
	private String publicationName = "";
	private double publicationPrice = 0.00;
	private String publicationDescription = "";
	private String publicationFrequency = "";

	public Publication(String pubName, double pubPrice, String pubDescription, String pubFrequency)
			throws PublicationExceptionHandler {

		pubId = 0;
		this.publicationName = pubName;
		this.publicationPrice = pubPrice;
		this.publicationDescription = pubDescription;
		this.publicationFrequency = pubFrequency;

		try {
			validatePublicationName(pubName);
			validatePublicationPrice(pubPrice);
			validatePublicationDescription(pubDescription);
			validatePublicationFrequency(pubFrequency);
		} catch (PublicationExceptionHandler e) {
			throw e;
		}

	}
	
	static String validatePublicationName(String pubName) throws PublicationExceptionHandler {
		if (pubName.isBlank() || pubName.isEmpty()) {
			throw new PublicationExceptionHandler("Publication Name invalid, enter a name");
		} else if (pubName.length() < 6) {
			throw new PublicationExceptionHandler("Publication Name does not meet minimum length characters (must be greater than 5)");
		} else if (pubName.length() > 21) {
			throw new PublicationExceptionHandler("Publication Name exceeds maximum length requirements (must be less than 22)");
		} else {
			return "Publication Name accepted";
		}
	}

	static String validatePublicationPrice(Double pubPrice) throws PublicationExceptionHandler {

		if (pubPrice == 0.0 || pubPrice == null) {
			throw new PublicationExceptionHandler("Publication Price invalid, enter a price");
		} else if (pubPrice > 4.00) {
			throw new PublicationExceptionHandler("Publication Price exceeds maximum value(must be less than �4.01)");
		} else if (pubPrice < 2.00) {
			throw new PublicationExceptionHandler("Publication Price does not meet minimum value(must be greater than �1.99)");
		} else
			return "Publication Price accepted";
	}

	static String validatePublicationDescription(String pubDescription) throws PublicationExceptionHandler {

		if (pubDescription.isEmpty() || pubDescription.isBlank()) {
			throw new PublicationExceptionHandler("Publication Description invalid, enter a description");
		} else if (pubDescription.length() > 200) {
			throw new PublicationExceptionHandler("Publication Description exceeds maximum length characters (must be less than 201)");
		} else if (pubDescription.length() < 100) {
			throw new PublicationExceptionHandler("Publication Description does not meet minimum length characters(must be greater than 99)");
		}

		else {
			return "Publication Description accepted";
		}
	}

	static String validatePublicationFrequency(String pubFrequency) throws PublicationExceptionHandler {

		if (pubFrequency.isEmpty() || pubFrequency.isBlank()) {
			throw new PublicationExceptionHandler("No input detected. Please enter a frequency");
		} else if (pubFrequency.equals("daily")) {
			return "Publication Frequency has been set to daily";
		} else if (pubFrequency.equals("weekly")) {
			return "Publication Frequency has been set to weekly";
		} else if (pubFrequency.equals("monthly")) {
			return "Publication Frequency has been set to monthly";
		} else {
			throw new PublicationExceptionHandler("Publication Frequency is invalid, enter a valid");
		}
	}

	public String getPublicationName() {
		return publicationName;
	}

	public void setPublicationName(String publicationName) {
		this.publicationName = publicationName;
	}

	public double getPublicationPrice() {
		return publicationPrice;
	}

	public void setPublicationPrice(double publicationPrice) {
		this.publicationPrice = publicationPrice;
	}

	public String getPublicationDescription() {
		return publicationDescription;
	}

	public void setPublicationDescription(String publicationDescription) {
		this.publicationDescription = publicationDescription;
	}

	public String getPublicationFrequency() {
		return publicationFrequency;
	}

	public void setPublicationFrequency(String publicationFrequency) {
		this.publicationFrequency = publicationFrequency;
	}
	
	public int getPubId() {
		return pubId;
	}
	public void setPubId(int pubId) {
		this.pubId = pubId;
	}

}
