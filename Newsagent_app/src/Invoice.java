import java.text.SimpleDateFormat;
import java.util.Date;

public class Invoice {

	private Customer customer;
	private Publication publication;
	private String companyName="";
	private String companyEircode="";
	private double totalPrice=0.0;
	private int invoiceID;
	private Date date = new Date();
	private SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
	private String stringDate= DateFor.format(date);
	

	public Invoice(Customer customer, String compName, String compEircode, Publication publication, double tPrice) throws InvoiceExceptionHandler{
		invoiceID = 0;

		// Validate Input
		try {

			validateCustomerFirstName(customer.getName());
			validateCustomerLastName(customer.getSurname());
			validateCustomerEircode(customer.getEircode());
			validateCompanyName(compName);
			validateCompanyEircode(compEircode);
			validateItemName(publication.getPublicationName());
			validateTotalPrice(tPrice);

		} catch (InvoiceExceptionHandler e) {
			throw e;
		}

		this.customer = customer;
		this.companyName = compName;
		this.companyEircode = compEircode;
		this.publication = publication;
		this.totalPrice = tPrice;
	}
	
	public Invoice(Customer customer, String compName, String compEircode, Publication publication, double tPrice, String currentDate) throws InvoiceExceptionHandler{
		invoiceID = 0;

		// Validate Input
		try {

			validateCustomerFirstName(customer.getName());
			validateCustomerLastName(customer.getSurname());
			validateCustomerEircode(customer.getEircode());
			validateCompanyName(compName);
			validateCompanyEircode(compEircode);
			validateItemName(publication.getPublicationName());
			validateTotalPrice(tPrice);

		} catch (InvoiceExceptionHandler e) {
			throw e;
		}

		this.customer = customer;
		this.companyName = compName;
		this.companyEircode = compEircode;
		this.publication = publication;
		this.totalPrice = tPrice;
		this.stringDate = currentDate;
}
	
	public Invoice() throws InvoiceExceptionHandler{
			throw new RuntimeException("No code Written");
}

	public static String validateCustomerFirstName(String cusFirstName) throws InvoiceExceptionHandler{
		if (cusFirstName.isBlank() || cusFirstName.isEmpty()) {
			throw new InvoiceExceptionHandler("No input detected for Customer first name, please enter a name");
		} else if (cusFirstName.length() < 2) {
			throw new InvoiceExceptionHandler("Customer first name does not meet minimum requirements, please enter a valid name between 2-30 characters");
		} else if (cusFirstName.length() > 30) {
			throw new InvoiceExceptionHandler("Customer first name exceeds maximum requirements, please enter a valid name between 2-30 characters");
		} else {
			return "Customer First Name accepted";
		}
	}
	
	public static String validateCustomerLastName(String cusLastName) throws InvoiceExceptionHandler{
		if (cusLastName.isBlank() || cusLastName.isEmpty()) {
			throw new InvoiceExceptionHandler("No input detected for Customer last name, please enter a name");
		} else if (cusLastName.length() < 2) {
			throw new InvoiceExceptionHandler("Customer last name does not meet minimum requirements, please enter a valid name between 2-30 characters");
		} else if (cusLastName.length() > 30) {
			throw new InvoiceExceptionHandler("Customer last name exceeds maximum requirements, please enter a valid name between 2-30 characters");
		} else {
			return "Customer Last Name accepted";
		}
	}
	
	public static String validateCustomerEircode(String cusEircode) throws InvoiceExceptionHandler{
		if (cusEircode.isBlank() || cusEircode.isEmpty()) {
			throw new InvoiceExceptionHandler("No input detected for Customer eircode, please enter an eircode");
		} else if (cusEircode.length() < 7) {
			throw new InvoiceExceptionHandler("Customer eircode does not meet minimum requirements, please enter a valid eircode with 7-8 digits");
		} else if (cusEircode.length() > 8) {
			throw new InvoiceExceptionHandler("Customer eircode exceeds maximum requirements, please enter a valid eircode with 7-8 digits");
		} else {
			return "Customer eircode accepted";
		}
	}
	
	public static String validateCompanyName(String compName) throws InvoiceExceptionHandler{
		if (compName.isBlank() || compName.isEmpty()) {
			throw new InvoiceExceptionHandler("No input detected for Company name, please enter a name");
		} else if (compName.length() < 2) {
			throw new InvoiceExceptionHandler("Company name does not meet minimum requirements, please enter a valid name between 2-30 characters");
		} else if (compName.length() > 30) {
			throw new InvoiceExceptionHandler("Company name exceeds maximum requirements, please enter a valid name between 2-30 characters");
		} else {
			return "Company name accepted";
		}
	}
	
	public static String validateCompanyEircode(String compEircode) throws InvoiceExceptionHandler{
		if (compEircode.isBlank() || compEircode.isEmpty()) {
			throw new InvoiceExceptionHandler("No input detected for Company eircode, please enter an eircode");
		} else if (compEircode.length() < 7) {
			throw new InvoiceExceptionHandler("Company eircode does not meet minimum requirements, please enter a valid eircode between 7-8 digits");
		} else if (compEircode.length() > 8) {
			throw new InvoiceExceptionHandler("Company exceeds maximum requirements, please enter a valid eircode between 7-8 digits");
		} else {
			return "Company eircode accepted";
		}
	}
	
	public static String validateItemName(String iName) throws InvoiceExceptionHandler{
		if (iName.isBlank() || iName.isEmpty()) {
			throw new InvoiceExceptionHandler("Invalid item Name, please enter a valid name between 6-21 characters");
		} else if (iName.length() < 6) {
			throw new InvoiceExceptionHandler("Item name does not meet minimum requirements, enter a valid item name between 6-21 characters");
		} else if (iName.length() > 21) {
			throw new InvoiceExceptionHandler("Item name exceeds maximum requirements, please enter a valid item name between 6-21 characters");
		} else {
			return "Item name accepted";
		}
	}
	
	public static String validateTotalPrice(Double tPrice) throws InvoiceExceptionHandler{
		if (tPrice == 0.0 || tPrice == null) {
			throw new InvoiceExceptionHandler("Total price invalid, enter a total price");
		} else if (tPrice < 2.00) {
			throw new InvoiceExceptionHandler("Total price invalid, total price must be atleast �2.00 or higher");
		} else
			return "Total Price accepted";
	}
	
	public String getCustomerFirstName() {
		return customer.getName();
	}

	public void setCustomerFirstName(String customerFirstName) {
		customerFirstName = customer.getName();
	}

	public String getCustomerLastName() {
		return customer.getSurname();
	}

	public void setCustomerLastName(String customerLastName) {
		customerLastName = customer.getSurname();
	}

	public String getCustomerEircode() {
		return customer.getEircode();
	}

	public void setCustomerEircode(String customerEircode) {
		customerEircode = customer.getEircode();
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyEircode() {
		return companyEircode;
	}

	public void setCompanyEircode(String companyEircode) {
		this.companyEircode = companyEircode;
	}

	public String getItemName() {
		return publication.getPublicationName();
	}

	public void setItemName(String itemName) {
		itemName= publication.getPublicationName();
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}
	
	
	
}
