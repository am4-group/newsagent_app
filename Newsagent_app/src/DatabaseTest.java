import java.sql.DriverManager;
import junit.framework.TestCase;

public class DatabaseTest extends TestCase{
	
	//Test No: 1
	//Objective: Test the success of inserting data
	// Input(s): name = "Mary",  surname = "Veluz", phoneNumber = "0868719842", eircode =
	// "J91P411", age = 24, deliveryArea = "WEM"
	//Expected Output: Customer Details Saved
	public void testDatabase001 () throws Exception  {
			//create database object 
			Database dbObj = new Database();			
			//create customer object
			Customer testObj1 = new Customer("Mary", "Veluz", "0868719842", "J91P411", 24, "WEM", "Home");
			
			// insert data into 
			boolean insertResult = dbObj.insertCustomerDetails(testObj1);
			assertEquals(true, insertResult);
	}
	
	//Test No: 2
	//Objective: Test the failure of inserting data
	//Input(s):	Database connection  = null
	//Expected Output: 	PROGRAM TERMINATED - ERROR MESSAGE: connection null
	public void testDatabase002 () throws Exception  {
		Database dbObj = new Database();
		dbObj.setConnection();
		assertFalse(dbObj.insertCustomerDetails(null));

	}
	
	//Test No: 3
	//Objective: Test the success of updating customer data
	// Input(s): name = "Rose",  surname = "Abi Akl", phoneNumber = "0868719842", eircode =
	// "J91P411", age = 29, deliveryArea = "WEM"
	//Expected Output: Customer data updated
	public void testDatabase003 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		// update customer data
		boolean insertResult = dbObj.modifyCustomerById(2, "Rose", "Abi Akl", "0868719842", "J91P411", 29, "WEM");
		assertEquals(true, insertResult);
	}
	
	//Test No: 4
	//Objective: Test the success of retrieving all customers data
	//Input(s): N/A
	//Expected Output: All customer data in the table
	public void testDatabase004 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllCustomers()); 
	}
	
	//Test No: 5
	//Objective: Test the failure of retrieving data
	//Input(s): N/A
	//Expected Output: PROGRAM TERMINATED - ERROR MESSAGE: connection null
	public void testDatabase005 () throws Exception  {
		Database dbObj = new Database();
		dbObj.setConnection();
		assertEquals(null, dbObj.viewAllCustomers()); 
	}
	
	//Test No: 6
	//Objective: Test the success of deleting one customer by id
	//Input(s): 1
	//Expected Output:  Customer Deleted
	public void testDatabase006 () throws Exception  {
		Database dbObj = new Database();
		String deleteCustId = "1";
		boolean deleteResult = dbObj.deleteCustomerById(Integer.parseInt(deleteCustId));
		assertEquals(true, deleteResult);
	}
	
	//Test No: 7
	//Objective: Test the success of delete all customers data
	//Input(s):  -99
	//Expected Output: All customers deleted
	public void testDatabase007 () throws Exception  {
		Database dbObj = new Database();
		String deleteCustId = "-99";
		boolean deleteResult = dbObj.deleteCustomerById(Integer.parseInt(deleteCustId));
		assertEquals(true, deleteResult);
	}
	
	//Test No: 8 
	//Objective: Test the success of inserting publication data
	//Input(s): publicationName = "Irish Times" , publicationPrice = 2.00  ,
	//publicationDescription = "The Irish Times is an Irish daily broadsheet newspaper and online digital publication.", publicationFrequency = "daily"
	//Expected Output: 
	public void testDatabase008 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		//create customer object
		Publication testObj1 = new Publication("Irish Times", 2.00, "The Irish Times is an Irish daily broadsheet newspaper and online digital publication. It launched on 29 March 1859. ", "daily");
		
		// insert data into 
		boolean insertResult = dbObj.insertPublication(testObj1);
		assertEquals(true, insertResult);
	}
	
	//Test No: 9 
	//Objective: Test the success of modifying publication data
	//Input(s): publicationName = "Irish Times" , publicationPrice = 1.99  ,
	//publicationDescription = "The Irish Times is an Irish daily broadsheet newspaper and online digital publication.", publicationFrequency = "Weekly"
	//Expected Output:  Publication data updated
	public void testDatabase009 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		// update Publication data
		boolean insertResult = dbObj.modifyPublicationByID(1, "Irish Times", 1.99, "The Irish Times is an Irish Times broadsheet newspaper and online digital publication. It launched on 29 March 1859. ", "Weekly");
		assertEquals(true, insertResult);
	}
	
	//Test No: 10
	//Objective: Test the success of retrieving all publication data
	//Expected Output: All publication data in the table
	public void testDatabase010 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllPublications()); 
	}
	
	//Test No: 11
	//Objective: Test the success of retrieving one publication data
	//Expected Output: All publication data in the table
	public void testDatabase011 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewOnePublication(1)); 
	}
	
	//Test No: 12
	//Objective: Test the success of deleting one publication by id
	//Input(s): 1
	//Expected Output:  Publication Data Deleted
	public void testDatabase012 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deletePublicationrById(1);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 13
	//Objective: Test the success of deleting all publications
	//Input(s): -99
	//Expected Output:  Publication Data Deleted
	public void testDatabase013 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deletePublicationrById(-99);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 14
	//Objective: Test the success of modifying invoice data by ID
	//Input(s): 1
	//Expected Output:  Invoice updated
	public void testDatabase014() throws Exception{
		//create database object 
		Database dbObj = new Database();
		// update Invoice data
		boolean insertResult = dbObj.modifyInvoiceByID(1, "Mary", "Veluz","B43 DY56", "Irish Time", "B43 BH11", "Irish News" ,7.00, "19/03/2022" );
		assertEquals(true, insertResult);
	}
	

	//Test No: 15
	//Objective: Test the success of retrieving all invoice data
	//Expected Output: All invoice data in the table
	public void testDatabase015 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllInvoices()); 
	}
	
	//Test No: 16
	//Objective: Test the success of retrieving one invoice data
	//Expected Output: All invoice data in the table
	public void testDatabase016 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewOneInvoice(1)); 
	}
	
	//Test No: 17
	//Objective: Test the success of deleting one invoice by id
	//Input(s): 1
	//Expected Output:  invoice Data Deleted
	public void testDatabase017 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteInvoiceById(1);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 18
	//Objective: Test the success of deleting all invoice
	//Input(s): -99
	//Expected Output:  invoice Data Deleted
	public void testDatabase018 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteInvoiceById(-99);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 19 
	//Objective: Test the success of inserting deliveryArea data
	//Input(s): 
	//Expected Output: Success
	public void testDatabase019 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		//create deliveryArea object
		Deliveryarea testObj1 = new Deliveryarea("WEM", "N65");
			
		// insert data into 
		boolean insertResult = dbObj.insertDeliveryArea(testObj1);
		assertEquals(true, insertResult);
	}
	
	//Test No: 20
	//Objective: Test the success of updating deliveryArea data
	// Input(s):
	//Expected Output: Delivery Area updated
	public void testDatabase020 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		// update deliveryArea data
		boolean insertResult = dbObj.modifyDeliveryArea(11, "LIT", "H67");
		assertEquals(true, insertResult);
	}
	
	//Test No: 21
	//Objective: Test the success of retrieving all deliveryArea data
	//Expected Output: All deliveryArea data in the table
	public void testDatabase021 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllDeliveryAreas()); 
	}
	
	//Test No: 22
	//Objective: Test the success of retrieving one deliveryArea data
	//Expected Output:  One deliveryArea data in the table
	public void testDatabase022 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewOneDeliveryArea(11)); 
	}
	
	//Test No: 23
	//Objective: Test the success of deleting one deliveryArea by id
	//Input(s): 1
	//Expected Output:  deliveryArea Data Deleted
	public void testDatabase023 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryArearById(1);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 24
	//Objective: Test the success of deleting all deliveryArea
	//Input(s): -99
	//Expected Output:  deliveryArea Data Deleted
	public void testDatabase024 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryArearById(-99);
		assertEquals(true, deleteResult);
	}
	
	
	//Test No: 25 
	//Objective: Test the success of inserting deliveryDocket data
	//Input(s): 
	//Expected Output: Success
	public void testDatabase025 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		//create deliveryArea object
		Deliverydocket testObj1 = new Deliverydocket("Mary", "Veluz", "H32AS65", "Irish Times", "The Irish Times is an Irish daily broadsheet "
				+ "newspaper and online digital publication. It launched on 29 March 1859. ");
			
		// insert data into 
		boolean insertResult = dbObj.insertDeliveryDocket(testObj1);
		assertEquals(true, insertResult);
	}
	
	//Test No: 26
	//Objective: Test the success of updating Delivery docket data
	// Input(s):
	//Expected Output: Delivery docket updated
	public void testDatabase026 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		// update deliveryArea data
		boolean insertResult = dbObj.modifyDeliveryDocket(1, "Mary", "Veluz", "H32AS65", "Irish Independent", "The Irish Times is an Irish daily broadsheet "
				+ "newspaper and online digital publication. It launched on 29 March 1859. ");
		assertEquals(true, insertResult);
	}
	
	//Test No: 27
	//Objective: Test the success of retrieving all Delivery docket data
	//Expected Output: All Delivery docket data in the table
	public void testDatabase027 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllDeliveryDocket()); 
	}
	
	//Test No: 28
	//Objective: Test the success of retrieving one Delivery docket data
	//Expected Output:  One Delivery docket data in the table
	public void testDatabase028 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewOneDeliveryDocket(11)); 
	}
	
	//Test No: 29
	//Objective: Test the success of deleting one Delivery docket by id
	//Input(s): 1
	//Expected Output:  Delivery docket Data Deleted
	public void testDatabase029 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryDocketById(1);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 30
	//Objective: Test the success of deleting all Delivery docket
	//Input(s): -99
	//Expected Output:  Delivery docket Data Deleted
	public void testDatabase030 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryDocketById(-99);
		assertEquals(true, deleteResult);
	}
	

	//Test No: 31 
	//Objective: Test the success of inserting deliveryPerson data
	//Input(s): 
	//Expected Output: Success
	public void testDatabase031 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		//create deliveryArea object
		Deliveryperson testObj1 = new Deliveryperson("Mary", "Veluz", "08672225555", "WEM");
			
		// insert data into 
		boolean insertResult = dbObj.insertDeliveryPerson(testObj1);
		assertEquals(true, insertResult);
	}
	
	//Test No: 32
	//Objective: Test the success of updating deliveryPerson data
	// Input(s):
	//Expected Output: deliveryPerson updated
	public void testDatabase032 () throws Exception  {
		//create database object 
		Database dbObj = new Database();
		// update deliveryArea data
		boolean insertResult = dbObj.modifyDeliveryPerson(1, "Rose", "White", "0868712222", "WEM");
		assertEquals(true, insertResult);
	}
	
	//Test No: 33
	//Objective: Test the success of retrieving all deliveryPerson data
	//Expected Output: All deliveryPerson data in the table
	public void testDatabase033 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewAllDeliveryPerson()); 
	}
	
	//Test No: 34
	//Objective: Test the success of retrieving one deliveryPerson data
	//Expected Output:  One deliveryPerson data in the table
	public void testDatabase034 () throws Exception  {
			Database dbObj = new Database();
			assertNotNull(dbObj.viewOneDeliveryPerson(1)); 
	}
	
	//Test No: 35
	//Objective: Test the success of deleting one deliveryPerson by id
	//Input(s): 1
	//Expected Output:  deliveryPerson Data Deleted
	public void testDatabase035 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryPersonById(1);
		assertEquals(true, deleteResult);
	}
	
	//Test No: 36
	//Objective: Test the success of deleting all deliveryPerson
	//Input(s): -99
	//Expected Output:  deliveryPerson Data Deleted
	public void testDatabase036 () throws Exception  {
		Database dbObj = new Database();
		boolean deleteResult = dbObj.deleteDeliveryPersonById(-99);
		assertEquals(true, deleteResult);
	}
	
	
	
}
