
public class Account {
	private int id;
	private String username;
	private String password;

	public Account(String username, String password) throws AccountExceptionHandler {
		id = 0;

		// Validate Input
		try {
			validateUsername(username);
			validatePassword(password);

		} catch (AccountExceptionHandler e) {
			throw e;
		}

		this.username = username;
		this.password = password;
	}

	public static String validateUsername(String username) throws AccountExceptionHandler {
		if (username.isBlank() || username.isEmpty())
			throw new AccountExceptionHandler("Username not entered");
		else if (username.length() < 2)
			throw new AccountExceptionHandler("Username does not meet minimum length requirements");
		else if (username.length() > 20)
			throw new AccountExceptionHandler("Username exceeds maximum length requirements");
		else
			return "Username valid";
	}

	public static String validatePassword(String password) throws AccountExceptionHandler {
		if (password.isBlank() || password.isEmpty())
			throw new AccountExceptionHandler("Password not entered");
		else if (password.length() < 2)
			throw new AccountExceptionHandler("Password does not meet minimum length requirements");
		else if (password.length() > 20)
			throw new AccountExceptionHandler("Password exceeds maximum length requirements");
		else
			return "Password valid";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}