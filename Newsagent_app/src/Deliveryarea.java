
public class Deliveryarea{
	
	private int id;
	private String areaname;
	private String routingkey;
	
	public Deliveryarea() throws DeliveryareaExceptionHandler{
		throw new RuntimeException("No Code Written");
	}
	
	public Deliveryarea(String areaname, String routingkey)
		throws DeliveryareaExceptionHandler{
		
		id=0;
		
		try {
			validateareaname(areaname);
			validateroutingkey(routingkey);
			
		}catch (DeliveryareaExceptionHandler e) {
			
			throw e;
		}
		
		this.areaname = areaname;
		this.routingkey = routingkey;
	
	}
	
	public static String validateareaname(String areaname) throws DeliveryareaExceptionHandler{
		if(areaname.isBlank() || areaname.isEmpty())
			throw new DeliveryareaExceptionHandler("Area name is invalid, no value entered");
		else if(areaname.length() < 3)
			throw new DeliveryareaExceptionHandler("Area name character length is too short");
		else if(areaname.length() >3)
			throw new DeliveryareaExceptionHandler("Area name is invalid character length is too long");
		else
			return "Area name is accepted";
		
	}
	
	public static String validateroutingkey(String routingkey) throws DeliveryareaExceptionHandler{
		if(routingkey.isBlank() || routingkey.isEmpty())
			throw new DeliveryareaExceptionHandler("Routing key is invalid, no value entered");
		else if(routingkey.length() < 3)
			throw new DeliveryareaExceptionHandler("Routing key character length is too short");
		else if(routingkey.length() > 3)
			throw new DeliveryareaExceptionHandler("Routing key is invalid character length is too long");
		else
			return "Routing key is accepted";
	}
	
	public String getareaname() {
		return areaname;
	}
	
	public void setareaname(String areaname) {
		this.areaname = areaname;
	}
	
	public String getroutingkey() {
		return routingkey;
	}
	
	public void setroutingkey(String routingkey) {
		this.routingkey = routingkey;
	}
	
	public int getid() {
		return id;
	}
	
	public void setid() {
		this.id = id;
	}
	
}