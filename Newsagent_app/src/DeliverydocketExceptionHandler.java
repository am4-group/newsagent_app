
public class DeliverydocketExceptionHandler extends Exception {
	
	String message;
	
	public DeliverydocketExceptionHandler(String errMessage) {
		
		message = errMessage;
	}
	
	public String getMessage() {
		
		return message;
	}

}