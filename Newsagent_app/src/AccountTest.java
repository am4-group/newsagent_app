import junit.framework.TestCase;

public class AccountTest extends TestCase {
	// Username Partitions: blank-1 2-20 21 ...
	// Password Partitions: blank-1 2-20 21 ...

	// Test No.: 1
	// Objective: Verify a correct password and username combo will make a new account
	// Input(s): Username = "Ko", Password = "Ko"
	// Expected Output: Account created
	public void testvalidateAccount001() throws AccountExceptionHandler {
		try {
			Account testObj = new Account("Ko", "Ko");
			assertEquals("Ko", testObj.getUsername());
			assertEquals("Ko", testObj.getPassword());
		} catch (AccountExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 2
	// Objective: Verify an incorrect password will throw an exception
	// Input(s): Username = "QLPSJMQZWUBHTJNLHNKK", Password =
	// "QLPSJMQZWUBHTJNLHNKKJ"
	// Expected Output: Password exceeds maximum length requirements
	public void testvalidateAccount002() throws AccountExceptionHandler {
		try {
			Account testObj = new Account("QLPSJMQZWUBHTJNLHNKK", "QLPSJMQZWUBHTJNLHNKKJ");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Password exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 3
	// Objective: Verify an incorrect username will throw an exception
	// Input(s): Username = "QLPSJMQZWUBHTJNLHNKKJ", Password =
	// "QLPSJMQZWUBHTJNLHNKK"
	// Expected Output: Username exceeds maximum length requirements
	public void testvalidateAccount003() throws AccountExceptionHandler {
		try {
			Account testObj = new Account("QLPSJMQZWUBHTJNLHNKKJ", "QLPSJMQZWUBHTJNLHNKK");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {

		}
	}

	// Test No.: 4
	// Objective: Verify an incorrect username and password will throw an exception
	// Input(s): Username = "", Password = ""
	// Expected Output: Username not entered
	public void testvalidateAccount004() throws AccountExceptionHandler {
		try {
			Account testObj = new Account("", "");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {

		}
	}

	// Test No.: 5
	// Objective: Verify an incorrect username will throw an exception
	// Input(s): Username = "K", Password = "K"
	// Expected Output: Username does not meet minimum length requirements
	public void testvalidateAccount005() throws AccountExceptionHandler {
		try {
			Account testObj = new Account("K", "K");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Username does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 6
	// Objective: Verify an incorrect username will throw an exception
	// Input(s): Username = ""
	// Expected Output: Username not entered
	public void testvalidateUsername001() throws AccountExceptionHandler {
		try {
			Account.validateUsername("");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Username not entered", e.getMessage());
		}
	}

	// Test No.: 7
	// Objective: Verify an incorrect username will throw an exception
	// Input(s): Username = "K"
	// Expected Output: Username does not meet minimum length requirements
	public void testvalidateUsername002() throws AccountExceptionHandler {
		try {
			Account.validateUsername("K");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Username does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 8
	// Objective: Verify a correct username will validate
	// Input(s): Username = "KO"
	// Expected Output: Username valid
	public void testvalidateUsername003() throws AccountExceptionHandler {
		try {
			String result = Account.validateUsername("KO");
			assertEquals("Username valid", result);
		} catch (AccountExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 9
	// Objective: Verify a correct username will validate
	// Input(s): Username = "QLPSJMQZWUBHTJNLHNKK"
	// Expected Output: Username valid
	public void testvalidateUsername004() throws AccountExceptionHandler {
		try {
			String result = Account.validateUsername("QLPSJMQZWUBHTJNLHNKK");
			assertEquals("Username valid", result);
		} catch (AccountExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 11
	// Objective: Verify an incorrect username will throw an exception
	// Input(s): Username = "QLPSJMQZWUBHTJNLHNKKJ"
	// Expected Output: Username exceeds maximum length requirements
	public void testvalidateUsername005() throws AccountExceptionHandler {
		try {
			Account.validateUsername("QLPSJMQZWUBHTJNLHNKKJ");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Username exceeds maximum length requirements", e.getMessage());
		}
	}

	// Test No.: 13
	// Objective: Verify an incorrect password will throw an exception
	// Input(s): Password = ""
	// Expected Output: Password not entered
	public void testvalidatePassword001() throws AccountExceptionHandler {
		try {
			Account.validatePassword("");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Password not entered", e.getMessage());
		}
	}

	// Test No.: 14
	// Objective: Verify an incorrect password will throw an exception
	// Input(s): Password = ""
	// Expected Output: Password does not meet minimum length requirements
	public void testvalidatePassword002() throws AccountExceptionHandler {
		try {
			Account.validatePassword("K");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Password does not meet minimum length requirements", e.getMessage());
		}
	}

	// Test No.: 15
	// Objective: Verify a correct password will validate
	// Input(s): Password = "KO"
	// Expected Output: Password valid
	public void testvalidatePassword003() throws AccountExceptionHandler {
		try {
			String result = Account.validatePassword("KO");
			assertEquals("Password valid", result);
		} catch (AccountExceptionHandler e) {
			fail("Exception not expected");
		}
	}

	// Test No.: 16
	// Objective: Verify a correct password will validate
	// Input(s): Password = "QLPSJMQZWUBHTJNLHNKK"
	// Expected Output: Password valid
	public void testvalidatePassword004() throws AccountExceptionHandler {
		try {
			String result = Account.validatePassword("QLPSJMQZWUBHTJNLHNKK");
			assertEquals("Password valid", result);
		} catch (AccountExceptionHandler e) {
			fail("Exception expected");
		}
	}

	// Test No.: 17
	// Objective: Verify an incorrect password will throw an exception
	// Input(s): Password = "QLPSJMQZWUBHTJNLHNKKJ"
	// Expected Output: Password exceeds maximum length requirements
	public void testvalidatePassword005() throws AccountExceptionHandler {
		try {
			Account.validatePassword("QLPSJMQZWUBHTJNLHNKKJ");
			fail("Exception expected");
		} catch (AccountExceptionHandler e) {
			assertEquals("Password exceeds maximum length requirements", e.getMessage());
		}
	}
}
